package ch.briggen.bfh.sparklist.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.LocalDate;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author SmartBooks
 */

class PlattformItemTest {

    @Test
    void testEmptyConstructorYieldsEmptyObject() {
        PlattformItem plattformItem = new PlattformItem();
        BenutzerItem benutzerItem = new BenutzerItem();
        BuchItem buchItem = new BuchItem();
        HochschulItem hochschulItem = new HochschulItem();
        plattformItem.setBenutzerItem(benutzerItem);
        plattformItem.setBuchItem(buchItem);
        plattformItem.setHsItem(hochschulItem);

        assertThat("itemID", plattformItem.getItemID(), is(equalTo(0)));
        assertThat("benutzer", plattformItem.getBenutzerItem(), is(equalTo(benutzerItem)));
        assertThat("buchItem", plattformItem.getBuchItem(), is(equalTo(buchItem)));
        assertThat("hsItem", plattformItem.getHsItem(), is(equalTo(hochschulItem)));
        assertThat("verkauft", plattformItem.getVerkauft(), is(equalTo(0)));
    }

    @ParameterizedTest
    @CsvSource({"1, 0", "0, 1", "-1, -1"})
    void testConstructorAssignsAllFields(int itemID, int verkauft) {
        BenutzerItem benutzerItem = new BenutzerItem(1,"test@user.ch","TestUser");
        BuchItem buchItem = new BuchItem(1, "Title", "ISBN", "Author", LocalDate.parse("1998-10-04"), 1.0, 20.0);
        StudiengangItem studiengangItem = new StudiengangItem(1,"Wirtschaftsinformatik");
        HochschulItem hochschulItem = new HochschulItem(1, "Berner Fachhochschule", "Wirtschaft", "Bern", studiengangItem);
        PlattformItem plattformItem = new PlattformItem(itemID, benutzerItem, buchItem, hochschulItem, verkauft);

        assertThat("itemID", plattformItem.getItemID(), is(equalTo(itemID)));
        assertThat("benutzer", plattformItem.getBenutzerItem(), is(equalTo(benutzerItem)));
        assertThat("buchItem", plattformItem.getBuchItem(), is(equalTo(buchItem)));
        assertThat("hsItem", plattformItem.getHsItem(), is(equalTo(hochschulItem)));
        assertThat("verkauft", plattformItem.getVerkauft(), is(equalTo(verkauft)));
    }

    @ParameterizedTest
    @CsvSource({"1, 0", "0, 1", "-1, -1"})
    void testSetters(int itemID, int verkauft) {
        BenutzerItem benutzerItem = new BenutzerItem(1,"test@user.ch","TestUser");
        BuchItem buchItem = new BuchItem(1, "Title", "ISBN", "Author", LocalDate.parse("1998-10-04"), 1.0, 20.0);
        StudiengangItem studiengangItem = new StudiengangItem(1,"Wirtschaftsinformatik");
        HochschulItem hochschulItem = new HochschulItem(1, "Berner Fachhochschule", "Wirtschaft", "Bern", studiengangItem);
        PlattformItem plattformItem = new PlattformItem();

        plattformItem.setItemId(itemID);
        plattformItem.setBenutzerItem(benutzerItem);
        plattformItem.setBuchItem(buchItem);
        plattformItem.setHsItem(hochschulItem);
        plattformItem.setVerkauft(verkauft);

        assertThat("itemID", plattformItem.getItemID(), is(equalTo(itemID)));
        assertThat("benutzer", plattformItem.getBenutzerItem(), is(equalTo(benutzerItem)));
        assertThat("buchItem", plattformItem.getBuchItem(), is(equalTo(buchItem)));
        assertThat("hsItem", plattformItem.getHsItem(), is(equalTo(hochschulItem)));
        assertThat("verkauft", plattformItem.getVerkauft(), is(equalTo(verkauft)));
    }

    @ParameterizedTest
    @CsvSource({"1, 0", "0, 1", "-1, -1"})
    void testIsEqual(int itemID, int verkauft) {
        BenutzerItem benutzerItem = new BenutzerItem(1,"test@user.ch","TestUser");
        BuchItem buchItem = new BuchItem(1, "Title", "ISBN", "Author", LocalDate.parse("1998-10-04"), 1.0, 20.0);
        StudiengangItem studiengangItem = new StudiengangItem(1,"Wirtschaftsinformatik");
        HochschulItem hochschulItem = new HochschulItem(1, "Berner Fachhochschule", "Wirtschaft", "Bern", studiengangItem);

        PlattformItem plattformItem1 = new PlattformItem(itemID, benutzerItem, buchItem, hochschulItem, verkauft);
        PlattformItem plattformItem2 = new PlattformItem(itemID, benutzerItem, buchItem, hochschulItem, verkauft);
        PlattformItem plattformItem3 = new PlattformItem(itemID, benutzerItem, buchItem, new HochschulItem(), verkauft);

        assertThat("isEqual", plattformItem1.isEqual(plattformItem2), is(equalTo(true)));
        assertThat("isEqual", plattformItem1.isEqual(plattformItem3), is(equalTo(false)));
    }

    @Test
    void testToString() {
        PlattformItem plattformItem = new PlattformItem();
        System.out.println(plattformItem.toString());
        assertThat("toString smoke test", plattformItem.toString(), not(nullValue()));
    }

}
