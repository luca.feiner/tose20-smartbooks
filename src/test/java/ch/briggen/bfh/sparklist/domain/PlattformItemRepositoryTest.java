package ch.briggen.bfh.sparklist.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Collection;
import java.util.ArrayList;
import java.util.NoSuchElementException;

import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDataSourceForTest;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author SmartBooks
 */

public class PlattformItemRepositoryTest {

    PlattformItemRepository repo = null;


    private static void populateRepo(PlattformItemRepository r) {

        for(int i = 0; i <10; ++i) {
            BenutzerItem benutzerItem = new BenutzerItem(i,"test@user.ch","TestUser");
            BuchItem buchItem = new BuchItem(i, "Title", "ISBN", "Author", LocalDate.parse("1998-10-04"), 1.0, 20.0);
            StudiengangItem studiengangItem = new StudiengangItem(i,"Wirtschaftsinformatik" + i);
            HochschulItem hochschulItem = new HochschulItem(i, "Berner Fachhochschule" + i, "Wirtschaft"+i, "Bern", studiengangItem);
            PlattformItem plattformItem = new PlattformItem(i, benutzerItem, buchItem, hochschulItem, 0);

            r.insert(plattformItem);
        }
    }

    @BeforeEach
    void setUp() throws Exception {
        String randomName = "Test@"+System.currentTimeMillis();
        initDataSourceForTest(randomName);
        initDB();
        repo = new PlattformItemRepository();
    }

    @Test
    void testEmptyDB() {
        assertThat("New DB must be empty",repo.getAll().isEmpty(),is(true));
    }

    @Test
    void testPopulatedDB()
    {
        populateRepo(repo);
        assertThat("Freshly populated DB must hold 10 values",repo.getAll().size(),is(10));
    }

    @Test
    void testInsertItems()
    {
        BenutzerItem benutzerItem = new BenutzerItem(1,"test@user.ch","TESTUSER");
        BuchItem buchItem = new BuchItem(1, "TITEL", "ISBN", "AUTHOR", LocalDate.parse("1998-10-04"), 1.0, 20.0);
        StudiengangItem studiengangItem = new StudiengangItem(1,"WIRTSCHAFTSINFORMATIK");
        HochschulItem hochschulItem = new HochschulItem(1, "BERNER FACHHOCHSCHULE", "WIRTSCHAFT", "BERN", studiengangItem);
        PlattformItem plattformItem = new PlattformItem(1, benutzerItem, buchItem, hochschulItem, 0);

        populateRepo(repo);

        int dbId = (int) repo.insert(plattformItem);
        PlattformItem fromDB = repo.getById(dbId);
        assertThat("id = id", fromDB.getItemID(),is(dbId) );
        assertThat("benutzerItem = benutzerItem", fromDB.isEqual(plattformItem), is(equalTo(true)));

    }

    @Test
    void testUpdateItems()
    {
        BenutzerItem benutzerItem = new BenutzerItem(1,"test@user.ch","TESTUSER");
        BuchItem buchItem = new BuchItem(1, "TITEL", "ISBN", "AUTHOR", LocalDate.parse("1998-10-04"), 1.0, 20.0);
        StudiengangItem studiengangItem = new StudiengangItem(1,"WIRTSCHAFTSINFORMATIK");
        HochschulItem hochschulItem = new HochschulItem(1, "BERNER FACHHOCHSCHULE", "WIRTSCHAFT", "BERN", studiengangItem);
        PlattformItem plattformItem = new PlattformItem(1, benutzerItem, buchItem, hochschulItem, 0);

        populateRepo(repo);

        int dbId = (int) repo.insert(plattformItem);
        plattformItem.getBenutzerItem().setBenutzerName("NAME");
        plattformItem.getBuchItem().setVersion(2.0);
        plattformItem.getHsItem().getSgItem().setSgName("STUDIENGANG");
        plattformItem.getHsItem().setAbteilung("ABTEILUNG");
        plattformItem.setVerkauft(1);
        repo.update(plattformItem);

        PlattformItem itemFromDB = repo.getById(dbId);
        assertThat("itemID = itemID", itemFromDB.getItemID(), is(dbId));
        assertThat("Item = Item", itemFromDB.getBuchItem().getVersion(), is(plattformItem.getBuchItem().getVersion()));
        assertThat("Item = Item", itemFromDB.isEqual(plattformItem), is(equalTo(true)));
    }


    @Test
    void testDeleteItems()
    {
        BenutzerItem benutzerItem = new BenutzerItem(1,"test@user.ch","TESTUSER");
        BuchItem buchItem = new BuchItem(1, "TITEL", "ISBN", "AUTHOR", LocalDate.parse("1998-10-04"), 1.0, 20.0);
        StudiengangItem studiengangItem = new StudiengangItem(1,"WIRTSCHAFTSINFORMATIK");
        HochschulItem hochschulItem = new HochschulItem(1, "BERNER FACHHOCHSCHULE", "WIRTSCHAFT", "BERN", studiengangItem);
        PlattformItem plattformItem = new PlattformItem(1, benutzerItem, buchItem, hochschulItem, 0);

        populateRepo(repo);

        int dbId = (int) repo.insert(plattformItem);
        PlattformItem fromDB = repo.getById(dbId);
        assertThat("Item was written to DB", fromDB, not(nullValue()));
        repo.delete(dbId);
        assertThrows(NoSuchElementException.class, ()->{repo.getById(dbId);},"Item should have been deleted");

    }

    @Test
    void testSearchItems() {
        BenutzerItem benutzerItem = new BenutzerItem(1,"test@user.ch","TESTUSER");
        BuchItem buchItem = new BuchItem(1, "TITEL", "ISBN", "AUTHOR", LocalDate.parse("1998-10-04"), 1.0, 20.0);
        StudiengangItem studiengangItem = new StudiengangItem(1,"WIRTSCHAFTSINFORMATIK");
        HochschulItem hochschulItem = new HochschulItem(1, "BERNER FACHHOCHSCHULE", "WIRTSCHAFT", "BERN", studiengangItem);
        PlattformItem plattformItemOrig = new PlattformItem(1, benutzerItem, buchItem, hochschulItem, 0);

        int dbId = (int) repo.insert(plattformItemOrig);

        // Testen aller möglichen Suchkriterien-Kombinationen
        for(int i = 0; i < 19; i++) {
            BenutzerItem benutzerItem2 = new BenutzerItem(1,"test@user.ch","TESTUSER");
            BuchItem buchItem2 = new BuchItem(1, "TITEL", "ISBN", "AUTHOR", LocalDate.parse("1998-10-04"), 1.0, 20.0);
            StudiengangItem studiengangItem2 = new StudiengangItem(1,"WIRTSCHAFTSINFORMATIK");
            HochschulItem hochschulItem2 = new HochschulItem(1, "BERNER FACHHOCHSCHULE", "WIRTSCHAFT", "BERN", studiengangItem2);
            PlattformItem pI = new PlattformItem(1, benutzerItem2, buchItem2, hochschulItem2, 0);
            Collection<PlattformItem> dbItems;
            switch (i){
                case 0:
                    dbItems = repo.search(pI);
                    break;
                case 1:
                    pI.getHsItem().getSgItem().setSgName("");
                    dbItems = repo.search(pI);
                    break;
                case 2:
                    pI.getHsItem().setHsName("");
                    dbItems = repo.search(pI);
                    break;
                case 3:
                    pI.getHsItem().setHsName("");
                    pI.getHsItem().getSgItem().setSgName("");
                    dbItems = repo.search(pI);
                    break;
                case 4:
                    pI.getBuchItem().setISBN("");
                    dbItems = repo.search(pI);
                    break;
                case 5:
                    pI.getBuchItem().setISBN("");
                    pI.getHsItem().getSgItem().setSgName("");
                    dbItems = repo.search(pI);
                    break;
                case 6:
                    pI.getBuchItem().setISBN("");
                    pI.getHsItem().setHsName("");
                    dbItems = repo.search(pI);
                    break;
                case 7:
                    pI.getBuchItem().setISBN("");
                    pI.getHsItem().setHsName("");
                    pI.getHsItem().getSgItem().setSgName("");
                    dbItems = repo.search(pI);
                    break;
                case 8:
                    pI.getBuchItem().setTitel("");
                    dbItems = repo.search(pI);
                    break;
                case 9:
                    pI.getBuchItem().setTitel("");
                    pI.getHsItem().getSgItem().setSgName("");
                    dbItems = repo.search(pI);
                    break;
                case 10:
                    pI.getBuchItem().setTitel("");
                    pI.getHsItem().setHsName("");
                    dbItems = repo.search(pI);
                    break;
                case 11:
                    pI.getBuchItem().setTitel("");
                    pI.getHsItem().setHsName("");
                    pI.getHsItem().getSgItem().setSgName("");
                    dbItems = repo.search(pI);
                    break;
                case 12:
                    pI.getBuchItem().setTitel("");
                    pI.getBuchItem().setISBN("");
                    dbItems = repo.search(pI);
                    break;
                case 13:
                    pI.getBuchItem().setTitel("");
                    pI.getBuchItem().setISBN("");
                    pI.getHsItem().getSgItem().setSgName("");
                    dbItems = repo.search(pI);
                    break;
                case 14:
                    pI.getBuchItem().setTitel("");
                    pI.getBuchItem().setISBN("");
                    pI.getHsItem().setHsName("");
                    dbItems = repo.search(pI);
                    break;
                case 15:
                    pI.getBuchItem().setTitel("");
                    pI.getBuchItem().setISBN("");
                    pI.getHsItem().setHsName("");
                    pI.getHsItem().getSgItem().setSgName("");
                    dbItems = repo.search(pI);
                    break;
                case 16:
                    pI.getBuchItem().setTitel(null);
                    pI.getBuchItem().setISBN(null);
                    pI.getHsItem().getSgItem().setSgName("");
                    dbItems = repo.search(pI);
                    break;
                case 17:
                    pI.getBuchItem().setTitel(null);
                    pI.getBuchItem().setISBN(null);
                    pI.getHsItem().setHsName("");
                    dbItems = repo.search(pI);
                    break;
                case 18:
                    pI.getBuchItem().setTitel(null);
                    pI.getBuchItem().setISBN(null);
                    pI.getHsItem().setHsName("");
                    pI.getHsItem().getSgItem().setSgName("");
                    dbItems = repo.search(pI);
                    break;
                default:
                    dbItems = repo.search(pI);
                    break;
            }

            if(dbItems.isEmpty() == false) {
                for (PlattformItem plattformItem : dbItems) {
                    boolean test = false;
                    test = plattformItem.isEqual(plattformItemOrig);
                    assertThat("Erstelltes Item gefunden: ", plattformItem.isEqual(plattformItemOrig), is(equalTo(true)));
                }
            }
        }
    }

    @Test
    void testPopualateHS() {
        populateRepo(repo);
        ArrayList <String> result = new ArrayList<String>();
        result = repo.populateDropdownHs();

        assertThat("hsname = hsname", result.get(0), is(repo.getById(1).getHsItem().getHsName()));
        assertThat("hsname = hsname", result.get(1), is(repo.getById(2).getHsItem().getHsName()));
        assertThat("hsname = hsname", result.get(9), is(repo.getById(10).getHsItem().getHsName()));


    }


    @Test
    void testPopualateSg() {
        populateRepo(repo);
        ArrayList <String> result = new ArrayList<String>();
        result = repo.populateDropdownSg();

        assertThat("sgname = sgname", result.get(0), is(repo.getById(1).getHsItem().getSgItem().getSgName()));
        assertThat("sgname = sgname", result.get(1), is(repo.getById(2).getHsItem().getSgItem().getSgName()));
        assertThat("sgname = sgname", result.get(9), is(repo.getById(10).getHsItem().getSgItem().getSgName()));


    }

    @Test
    void testBuyBook() {
        populateRepo(repo);
        repo.buy(1);
        repo.buy(2);

        assertThat("verkauft = verkauft", 1, is(repo.getById(1).getVerkauft()));
        assertThat("verkauft = verkauft", 1, is(repo.getById(2).getVerkauft()));
        assertThat("verkauft = verkauft", 0, is(repo.getById(3).getVerkauft()));
    }

}
