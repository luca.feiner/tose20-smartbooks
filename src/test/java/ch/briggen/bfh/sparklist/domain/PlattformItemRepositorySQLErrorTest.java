package ch.briggen.bfh.sparklist.domain;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDataSourceForTest;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author SmartBooks
 */

public class PlattformItemRepositorySQLErrorTest {
    PlattformItemRepository repo = null;

    @BeforeEach
    void setUp() throws Exception {
        String randomName = "Test@"+System.currentTimeMillis();
        initDataSourceForTest(randomName);
        repo = new PlattformItemRepository();
    }

    @AfterEach
    void tearDown() throws Exception {
    }

    @Test
    void testSQLExceptionGetAll() {
        assertThrows(RepositoryException.class, ()->{repo.getAll();});
    }

    @Test
    void testSQLExceptionGetById() {
        assertThrows(RepositoryException.class, ()->{repo.getById(0);});
    }

    @Test
    void testSQLExceptionInsert() {
        assertThrows(RepositoryException.class, ()->{repo.insert(null);});
    }

    @Test
    void testSQLExceptionUpdate() {
        assertThrows(RepositoryException.class, ()->{repo.update(null);});
    }

    @Test
    void testSQLExceptionDelete() {
        assertThrows(RepositoryException.class, ()->{repo.delete(0);});
    }
}
