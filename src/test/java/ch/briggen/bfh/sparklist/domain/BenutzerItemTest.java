package ch.briggen.bfh.sparklist.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author SmartBooks
 */

public class BenutzerItemTest {
    @Test
    void testEmptyConstructorYieldsEmptyObject() {
        BenutzerItem benutzerItem = new BenutzerItem();
        assertThat("benutzerId", benutzerItem.getBenutzerId(), is(equalTo(0)));
        assertThat("email", benutzerItem.getEmail(), is(equalTo(null)));
        assertThat("name", benutzerItem.getBenutzerName(), is(equalTo(null)));

    }

    @ParameterizedTest
    @CsvSource({
            "1, test@user.ch, TestUser",
            "0, test0@user.ch, TestUser0",
            "-1, test-1@user.ch, TestUser-1",
    })
    void testConstructorAssignsAllFields(int benutzerId, String email, String name) {
        BenutzerItem benutzerItem = new BenutzerItem(benutzerId, email, name);

        assertThat("benutzerId", benutzerItem.getBenutzerId(), is(equalTo(benutzerId)));
        assertThat("email", benutzerItem.getEmail(), is(equalTo(email)));
        assertThat("name", benutzerItem.getBenutzerName(), is(equalTo(name)));
    }

    @ParameterizedTest
    @CsvSource({
            "1, test@user.ch, TestUser",
            "0, test0@user.ch, TestUser0",
            "-1, test-1@user.ch, TestUser-1",
    })
    void testSetters(int benutzerId, String email, String name) {
        BenutzerItem benutzerItem = new BenutzerItem();
        benutzerItem.setBenutzerId(benutzerId);
        benutzerItem.setEmail(email);
        benutzerItem.setBenutzerName(name);


        assertThat("benutzerId", benutzerItem.getBenutzerId(), is(equalTo(benutzerId)));
        assertThat("email",  benutzerItem.getEmail(), is(equalTo(email)));
        assertThat("name",benutzerItem.getBenutzerName(), is(equalTo(name)));
    }

    @ParameterizedTest
    @CsvSource({
            "1, test@user.ch, TestUser",
            "0, test0@user.ch, TestUser0",
            "-1, test-1@user.ch, TestUser-1",
    })
    void testIsEqual(int benutzerId, String email, String name) {
        BenutzerItem benutzerItem1 = new BenutzerItem(benutzerId, email, name);
        BenutzerItem benutzerItem2 = new BenutzerItem(benutzerId, email, name);
        BenutzerItem benutzerItem3 = new BenutzerItem(benutzerId, email, "name");

        assertThat("isEqual", benutzerItem1.isEqual(benutzerItem2), is(equalTo(true)));
        assertThat("isEqual", benutzerItem1.isEqual(benutzerItem3), is(equalTo(false)));
    }

    @Test
    void testToString() {
        BenutzerItem benutzerItem = new BenutzerItem();
        System.out.println(benutzerItem.toString());
        assertThat("toString smoke test", benutzerItem.toString(), not(nullValue()));
    }
}
