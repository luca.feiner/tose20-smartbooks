package ch.briggen.bfh.sparklist.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author SmartBooks
 */

class StudiengangItemTest {

    @Test
    void testEmptyConstructorYieldsEmptyObject() {
        StudiengangItem studiengangItem = new StudiengangItem();
        assertThat("sgID", studiengangItem.getSgId(), is(equalTo(0)));
        assertThat("sgName", studiengangItem.getSgName(), is(equalTo(null)));
    }

    @ParameterizedTest
    @CsvSource({"1,Wirtschaftsinformatik","0,BWL","-1,01238Hallo"})
    void testConstructorAssignsAllFields(int sgID,String sgName) {
        StudiengangItem studiengangItem = new StudiengangItem(sgID, sgName);
        assertThat("Id",studiengangItem.getSgId(),equalTo(sgID));
        assertThat("Name",studiengangItem.getSgName(),equalTo(sgName));
    }

    @ParameterizedTest
    @CsvSource({"1,Wirtschaftsinformatik","0,BWL","-1,01238Hallo"})
    void testSetters(int sgID,String sgName) {
        StudiengangItem studiengangItem = new StudiengangItem();
        studiengangItem.setSgId(sgID);
        studiengangItem.setSgName(sgName);
        assertThat("Id",studiengangItem.getSgId(),equalTo(sgID));
        assertThat("Name",studiengangItem.getSgName(),equalTo(sgName));
    }

    @ParameterizedTest
    @CsvSource({"1,Wirtschaftsinformatik","0,BWL","-1,01238Hallo"})
    void testIsEqual(int sgID,String sgName) {
        StudiengangItem studiengangItem1 = new StudiengangItem(sgID, sgName);
        StudiengangItem studiengangItem2 = new StudiengangItem(sgID, sgName);
        StudiengangItem studiengangItem3 = new StudiengangItem(sgID, "sgName");

        assertThat("isEqual", studiengangItem1.isEqual(studiengangItem2), is(equalTo(true)));
        assertThat("isEqual", studiengangItem1.isEqual(studiengangItem3), is(equalTo(false)));
    }

    @Test
    void testToString() {
        StudiengangItem studiengangItem = new StudiengangItem();
        System.out.println(studiengangItem.toString());
        assertThat("toString smoke test",studiengangItem.toString(),not(nullValue()));
    }

}
