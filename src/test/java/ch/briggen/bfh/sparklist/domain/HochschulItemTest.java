package ch.briggen.bfh.sparklist.domain;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author SmartBooks
 */

class HochschulItemTest {

    @Test
    void testEmptyConstructorYieldsEmptyObject() {
        HochschulItem hochschulItem = new HochschulItem();
        StudiengangItem studiengangItem = new StudiengangItem();
        hochschulItem.setSgItem(studiengangItem);
        assertThat("hsID", hochschulItem.getHsId(), is(equalTo(0)));
        assertThat("hsName", hochschulItem.getHsName(), is(equalTo(null)));
        assertThat("abteilung", hochschulItem.getAbteilung(), is(equalTo(null)));
        assertThat("ort", hochschulItem.getOrt(), is(equalTo(null)));
        assertThat("sgItem", hochschulItem.getSgItem(), is(equalTo(studiengangItem)));
    }

    @ParameterizedTest
    @CsvSource({
            "1, Berner Fachhochschule, Wirtschaft, Bern",
            "0, bsijwdb08921021, 0917e27hiosa, basiu0712",
            "-1, 01, 02, 03"
    })
    void testConstructorAssignsAllFields(int hsID,String hsName, String abteilung, String ort) {
        StudiengangItem studiengangItem = new StudiengangItem(1, "Wirtschaftsinformatik");
        HochschulItem hochschulItem = new HochschulItem(hsID, hsName, abteilung, ort, studiengangItem);
        assertThat("hsID", hochschulItem.getHsId(), is(equalTo(hsID)));
        assertThat("hsName", hochschulItem.getHsName(), is(equalTo(hsName)));
        assertThat("abteilung", hochschulItem.getAbteilung(), is(equalTo(abteilung)));
        assertThat("ort", hochschulItem.getOrt(), is(equalTo(ort)));
        assertThat("sgItem", hochschulItem.getSgItem(), is(equalTo(studiengangItem)));
    }

    @ParameterizedTest
    @CsvSource({
            "1, Berner Fachhochschule, Wirtschaft, Bern",
            "0, bsijwdb08921021, 0917e27hiosa, basiu0712",
            "-1, 01, 02, 03"
    })
    void testSetters(int hsID,String hsName, String abteilung, String ort) {
        StudiengangItem studiengangItem = new StudiengangItem(1, "Wirtschaftsinformatik");
        HochschulItem hochschulItem = new HochschulItem();
        hochschulItem.setHsId(hsID);
        hochschulItem.setHsName(hsName);
        hochschulItem.setAbteilung(abteilung);
        hochschulItem.setOrt(ort);
        hochschulItem.setSgItem(studiengangItem);
        assertThat("hsID", hochschulItem.getHsId(), is(equalTo(hsID)));
        assertThat("hsName", hochschulItem.getHsName(), is(equalTo(hsName)));
        assertThat("abteilung", hochschulItem.getAbteilung(), is(equalTo(abteilung)));
        assertThat("ort", hochschulItem.getOrt(), is(equalTo(ort)));
        assertThat("sgItem", hochschulItem.getSgItem(), is(equalTo(studiengangItem)));
    }

    @ParameterizedTest
    @CsvSource({
            "1, Berner Fachhochschule, Wirtschaft, Bern",
            "0, bsijwdb08921021, 0917e27hiosa, basiu0712",
            "-1, 01, 02, 03"
    })
    void testIsEqual(int hsID,String hsName, String abteilung, String ort) {
        StudiengangItem studiengangItem = new StudiengangItem(1, "Wirtschaftsinformatik");
        HochschulItem hochschulItem1 = new HochschulItem(hsID, hsName, abteilung, ort, studiengangItem);
        HochschulItem hochschulItem2 = new HochschulItem(hsID, hsName, abteilung, ort, studiengangItem);
        HochschulItem hochschulItem3 = new HochschulItem(hsID, hsName, "abteilung", ort, studiengangItem);

        assertThat("isEqual", hochschulItem1.isEqual(hochschulItem2), is(equalTo(true)));
        assertThat("isEqual", hochschulItem1.isEqual(hochschulItem3), is(equalTo(false)));
    }

    @Test
    void testToString() {
        HochschulItem hochschulItem = new HochschulItem();
        System.out.println(hochschulItem.toString());
        assertThat("toString smoke test",hochschulItem.toString(),not(nullValue()));
    }

}
