package ch.briggen.bfh.sparklist.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.LocalDate;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author SmartBooks
 */

class BuchItemTest {

    @Test
    void testEmptyConstructorYieldsEmptyObject() {
        BuchItem buchItem = new BuchItem();
        assertThat("buchId", buchItem.getBuchId(), is(equalTo(0)));
        assertThat("title", buchItem.getTitel(), is(equalTo(null)));
        assertThat("isbn", buchItem.getISBN(), is(equalTo(null)));
        assertThat("author", buchItem.getAuthor(), is(equalTo(null)));
        assertThat("veroeff_dat", buchItem.getVeroeff_dat(), is(equalTo(null)));
        assertThat("version", buchItem.getVersion(), is(equalTo(0.0)));
        assertThat("preis", buchItem.getPreis(), is(equalTo(0.0)));
    }

    @ParameterizedTest
    @CsvSource({
            "1, Title, ISBN, Author, 1998-10-04, 1.0, 20.0",
            "0, T0367, 0123, 456789, 2001-02-30, 0.0,  0.0",
            "-1, 0123, IS02, An0238, 2021-11-01, -1.0, -20.0"
    })
    void testConstructorAssignsAllFields(int buchId, String title, String isbn, String author, LocalDate veroeff_dat, double version, double preis) {
        BuchItem buchItem = new BuchItem(buchId, title, isbn, author, veroeff_dat, version, preis);

        assertThat("buchId", buchItem.getBuchId(), is(equalTo(buchId)));
        assertThat("title", buchItem.getTitel(), is(equalTo(title)));
        assertThat("isbn", buchItem.getISBN(), is(equalTo(isbn)));
        assertThat("author", buchItem.getAuthor(), is(equalTo(author)));
        assertThat("veroeff_dat", buchItem.getVeroeff_dat(), is(equalTo(veroeff_dat)));
        assertThat("version", buchItem.getVersion(), is(equalTo(version)));
        assertThat("preis", buchItem.getPreis(), is(equalTo(preis)));
    }

    @ParameterizedTest
    @CsvSource({
            "1, Title, ISBN, Author, 1998/10/04, 1.0, 20.0",
            "0, T0367, 0123, 456789, 2001/02/30, 0.0,  0.0",
            "-1, 0123, IS02, An0238, 2021/11/01, -1.0, -20.0"
    })
    void testSetters(int buchId, String title, String isbn, String author, LocalDate veroeff_dat, double version, double preis) {
        BuchItem buchItem = new BuchItem();
        buchItem.setBuchId(buchId);
        buchItem.setTitel(title);
        buchItem.setISBN(isbn);
        buchItem.setAuthor(author);
        buchItem.setVeroeff_dat(veroeff_dat);
        buchItem.setVersion(version);
        buchItem.setPreis(preis);

        assertThat("buchId", buchItem.getBuchId(), is(equalTo(buchId)));
        assertThat("title", buchItem.getTitel(), is(equalTo(title)));
        assertThat("isbn", buchItem.getISBN(), is(equalTo(isbn)));
        assertThat("author", buchItem.getAuthor(), is(equalTo(author)));
        assertThat("veroeff_dat", buchItem.getVeroeff_dat(), is(equalTo(veroeff_dat)));
        assertThat("version", buchItem.getVersion(), is(equalTo(version)));
        assertThat("preis", buchItem.getPreis(), is(equalTo(preis)));
    }

    @ParameterizedTest
    @CsvSource({
            "1, Title, ISBN, Author, 1998-10-04, 1.0, 20.0",
            "0, T0367, 0123, 456789, 2001-02-28, 0.0,  0.0",
            "-1, 0123, IS02, An0238, 2021-11-01, -1.0, -20.0"
    })
    void testIsEqual(int buchId, String title, String isbn, String author, LocalDate veroeff_dat, double version, double preis) {
        BuchItem buchItem1 = new BuchItem(buchId, title, isbn, author, veroeff_dat, version, preis);
        BuchItem buchItem2 = new BuchItem(buchId, title, isbn, author, veroeff_dat, version, preis);
        BuchItem buchItem3 = new BuchItem(buchId, title, isbn, author, veroeff_dat, version, 30);

        assertThat("isEqual", buchItem1.isEqual(buchItem2), is(equalTo(true)));
        assertThat("isEqual", buchItem1.isEqual(buchItem3), is(equalTo(false)));
    }


    @Test
    void testToString() {
        BuchItem buchItem = new BuchItem();
        System.out.println(buchItem.toString());
        assertThat("toString smoke test", buchItem.toString(), not(nullValue()));
    }

}
