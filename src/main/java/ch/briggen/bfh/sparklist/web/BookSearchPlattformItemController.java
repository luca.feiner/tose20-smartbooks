package ch.briggen.bfh.sparklist.web;

import ch.briggen.bfh.sparklist.domain.PlattformItem;
import ch.briggen.bfh.sparklist.domain.PlattformItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.ArrayList;
import java.util.HashMap;

public class BookSearchPlattformItemController implements TemplateViewRoute {

    private final Logger log = LoggerFactory.getLogger(BookCreateController.class);;
    private PlattformItemRepository repo = new PlattformItemRepository();

    /**
     * Führt die Suche durch und liefert die Resultate an die Suche-Seite zurück.
     * @param request
     * @param response
     * @return
     * @throws Exception
     */

    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        PlattformItem plattformItem = PlattformItemWebHelper.itemFromWeb(request);


        log.trace("POST /book/search mit plattformDetail " + plattformItem);

        HashMap<String, Object> model = new HashMap<String, Object>();
        ArrayList<String> HSItem = repo.populateDropdownHs();
        ArrayList<String> SGItem = repo.populateDropdownSg();
        //das Template itemDetail verwenden und dann "anzeigen".
        model.put("hs", HSItem);
        model.put("sg", SGItem);
        model.put("list", repo.search(plattformItem));

        return new ModelAndView(model, "BookSearchTemplate");
    }
}