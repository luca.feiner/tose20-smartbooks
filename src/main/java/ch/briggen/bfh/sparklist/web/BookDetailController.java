package ch.briggen.bfh.sparklist.web;

import ch.briggen.bfh.sparklist.domain.PlattformItem;
import ch.briggen.bfh.sparklist.domain.PlattformItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;
import java.util.HashMap;

public class BookDetailController implements TemplateViewRoute {

    private final Logger log = LoggerFactory.getLogger(ch.briggen.bfh.sparklist.web.BookManagementRootController.class);

    PlattformItemRepository repository = new PlattformItemRepository();

    /**
     * Liefert alle Informationen über ein Buch. Zeigt die Detailseite eines Buches an.
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        String idString = request.queryParams("id");
        HashMap<String, Object> model = new HashMap<String, Object>();

        log.trace("GET /book Detail  id " + idString);

        int id = Integer.parseInt(idString);
        PlattformItem i = repository.getById(id);
        model.put("PlattformItem", i);

        return new ModelAndView(model, "bookDetailTemplate");
    }
}
