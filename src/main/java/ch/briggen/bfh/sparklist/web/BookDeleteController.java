package ch.briggen.bfh.sparklist.web;

import ch.briggen.bfh.sparklist.domain.PlattformItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class BookDeleteController implements TemplateViewRoute {

    private final Logger log = LoggerFactory.getLogger(BookDeleteController.class);

    private PlattformItemRepository repo = new PlattformItemRepository();

    /**
     * Startet den Löschprozes und leitet auf die Startseite weiter
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        String id = request.queryParams("id");
        log.trace("GET /book/delete mit id " + id);
        int intId = Integer.parseInt(id);
        // Startet den Löschprozess
        repo.delete(intId);
        response.redirect("/");
        return null;
    }
}
