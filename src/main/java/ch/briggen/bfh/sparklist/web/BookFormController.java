package ch.briggen.bfh.sparklist.web;

import ch.briggen.bfh.sparklist.domain.PlattformItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;
import java.util.HashMap;

public class BookFormController implements TemplateViewRoute {

        private final Logger log = LoggerFactory.getLogger(ch.briggen.bfh.sparklist.web.BookManagementRootController.class);

        PlattformItemRepository repository = new PlattformItemRepository();

    /**
     * Leitet zum Erstell-Formular weiter
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
        @Override
        public ModelAndView handle(Request request, Response response) throws Exception {
            HashMap<String, Object> model = new HashMap<String, Object>();


            //das Template itemDetail verwenden und dann "anzeigen".
            return new ModelAndView(model, "bookCreateTemplate");
        }
    }
