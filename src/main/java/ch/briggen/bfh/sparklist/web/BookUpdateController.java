package ch.briggen.bfh.sparklist.web;

import ch.briggen.bfh.sparklist.domain.PlattformItem;
import ch.briggen.bfh.sparklist.domain.PlattformItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.HashMap;

public class BookUpdateController implements TemplateViewRoute {

    private final Logger log = LoggerFactory.getLogger(ch.briggen.bfh.sparklist.web.BookManagementRootController.class);

    PlattformItemRepository repository = new PlattformItemRepository();

    /**
     * Zeigt das Update-Formular an und liest das zu aktualisierende Buch aus.
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        String idString = request.queryParams("id");
        HashMap<String, Object> model = new HashMap<String, Object>();

        log.trace("GET /item für UPDATE mit id " + idString);
        //der Submit-Button ruft /item/update auf --> UPDATE
        model.put("postAction", "/item/update");

        //damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
        //dem Modell unter dem Namen "PlattformItem" hinzugefügt. itemDetal muss dem im HTML-Template verwendeten Namen entsprechen
        int id = Integer.parseInt(idString);
        PlattformItem i = repository.getById(id);
        model.put("PlattformItem", i);

        return new ModelAndView(model, "bookUpdateTemplate");
    }
}
