package ch.briggen.bfh.sparklist.web;

import ch.briggen.bfh.sparklist.domain.PlattformItem;
import ch.briggen.bfh.sparklist.domain.PlattformItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.Collection;
import java.util.HashMap;

public class ImpressumController implements TemplateViewRoute {

    @SuppressWarnings("unused")
    private final Logger log = LoggerFactory.getLogger(ImpressumController.class);

    PlattformItemRepository repository = new PlattformItemRepository();

    /**
     * Zeigt das Impressum an
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {

        //Items werden geladen und die Collection dann für das Template unter dem namen "list" bereitgestellt
        //Das Template muss dann auch den Namen "list" verwenden.
        HashMap<String, Collection<PlattformItem>> model = new HashMap<String, Collection<PlattformItem>>();
        model.put("list", repository.getAll());
        return new ModelAndView(model, "impressum");
    }
}
