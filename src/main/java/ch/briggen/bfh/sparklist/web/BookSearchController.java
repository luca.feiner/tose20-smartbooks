package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import ch.briggen.bfh.sparklist.domain.PlattformItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class BookSearchController implements TemplateViewRoute {

    private final Logger log = LoggerFactory.getLogger(BookSearchController.class);

    private PlattformItemRepository plattformItemRepo = new PlattformItemRepository();


    /**
     * Stellt die Suchwebseite bereit. Lädt Daten um in das Dropdown für das Formular zu laden.
     * @param request
     * @param response
     * @return
     * @throws Exception
     */

    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {

        HashMap<String, Object> model = new HashMap<String, Object>();
        ArrayList<String> HSItem = plattformItemRepo.populateDropdownHs();
        ArrayList<String> SGItem = plattformItemRepo.populateDropdownSg();
        //lädt die Variabel hs und sg mit den Hochschulen bzw. Studiengängen
        model.put("hs", HSItem);
        model.put("sg", SGItem);
        return new ModelAndView(model, "bookSearchTemplate");
    }
}
