package ch.briggen.bfh.sparklist.web;

import ch.briggen.bfh.sparklist.domain.PlattformItem;
import ch.briggen.bfh.sparklist.domain.PlattformItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;


public class BookManagementRootController implements TemplateViewRoute{

    private final Logger log = LoggerFactory.getLogger(BookManagementRootController.class);

    PlattformItemRepository repository = new PlattformItemRepository();

    /**
     * Die Darstellung der Startseite, lädt alle Bücher und stellt die Informationen für die Filterfunktion zur Verfügung
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {

        //Stellt Model zur Verfügung
        HashMap<String, Object> model = new HashMap<String, Object>();
        // Lädt die Studiengänge und Hochschulen in ArrayLists um diese im Filter zu verwenden
        ArrayList<String> HSItem = repository.populateDropdownHs();
        ArrayList<String> SGItem = repository.populateDropdownSg();
        //das Template itemDetail verwenden und dann "anzeigen".
        model.put("hs", HSItem);
        model.put("sg", SGItem);
        model.put("list", repository.getAll());
        return new ModelAndView(model, "bookOverviewTemplate");
    }
}
