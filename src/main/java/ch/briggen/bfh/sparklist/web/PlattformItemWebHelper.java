package ch.briggen.bfh.sparklist.web;

import ch.briggen.bfh.sparklist.domain.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;

import java.time.LocalDate;

public class PlattformItemWebHelper {

        @SuppressWarnings("unused")
        private final static Logger log = LoggerFactory.getLogger(ch.briggen.bfh.sparklist.web.PlattformItemWebHelper.class);

    /**
     * Bereitet die Daten vor damit sie im in der Klasse PlattformItemRepository verarbeitet werden können.
     * @param request
     * @return
     */
    public static PlattformItem itemFromWeb(Request request)
        {

            try {
                int itemId = 0;
                int sgId = 0;
                int hsId = 0;
                int benutzerId = 0;
                int buchId = 0;

                // Überprüft ob Ids mitgegeben werden
                if(!(request.queryParams("PlattformItem.id").equals("null"))){
                    itemId = Integer.parseInt(request.queryParams("PlattformItem.id"));
                    sgId = Integer.parseInt(request.queryParams("hsItem.sgItem.sgID"));
                    hsId = Integer.parseInt(request.queryParams("hsItem.hsID"));
                    benutzerId = Integer.parseInt(request.queryParams("benutzerItem.benutzerId"));
                    buchId = Integer.parseInt(request.queryParams("buchItem.buchID"));
                }
                // Bereitet das Benutzerobjekt vor
                BenutzerItem bn = new BenutzerItem(
                        benutzerId,
                        request.queryParams("PlattformItem.email"),
                        request.queryParams("PlattformItem.benutzername")
                );
                // Bereitet das Studiengang Objekt vor
                StudiengangItem sgItem = new StudiengangItem(
                        sgId,
                        request.queryParams("PlattformItem.studiengang")
                );
                // Bereitet das HochschulItem Objekt vor
                HochschulItem hsItem = new HochschulItem(
                        hsId,
                        request.queryParams("PlattformItem.hochschule"),
                        request.queryParams("PlattformItem.hs_abteilung"),
                        request.queryParams("PlattformItem.hs_ort"),
                        sgItem
                );

                // Setzt Werte in Date, Version und Preis ein, falls die Werte vom Formular NULL sind
                LocalDate date;
                if(request.queryParams("PlattformItem.veroeff_dat") == null){
                    date = null;
                } else {
                    date = LocalDate.parse(request.queryParams("PlattformItem.veroeff_dat"));
                }
                Double version;
                if(request.queryParams("PlattformItem.version") == null){
                    version = 0.0;
                } else {
                    version = Double.parseDouble(request.queryParams("PlattformItem.version"));
                }
                Double preis;
                if(request.queryParams("PlattformItem.veroeff_dat") == null){
                    preis = 0.0;
                } else {
                    preis = Double.parseDouble(request.queryParams("PlattformItem.preis"));
                }
                // Bereitet das BuchItem Objekt vor
                BuchItem bi = new BuchItem(
                        buchId,
                        request.queryParams("PlattformItem.titel"),
                        request.queryParams("PlattformItem.isbn"),
                        request.queryParams("PlattformItem.author"),
                        date,
                        version,
                        preis

                );
                // Bereitet das PlattformItem Objekt vor
                return new PlattformItem(
                        itemId,
                        bn,
                        bi,
                        hsItem,
                        0
                );

            } catch (Exception e){
                //TODO: Was passiert, wenn auslesen der Daten Fehlerhaft?
                log.trace(e.toString());
                e.printStackTrace();
                return new PlattformItem();
            }
        }

}