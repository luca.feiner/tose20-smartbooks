package ch.briggen.bfh.sparklist.web;

import ch.briggen.bfh.sparklist.domain.PlattformItem;
import ch.briggen.bfh.sparklist.domain.PlattformItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.HashMap;

public class BookBuyController implements TemplateViewRoute {

    private final Logger log = LoggerFactory.getLogger(BookCreateController.class);;
    private PlattformItemRepository repo = new PlattformItemRepository();

    /**
     * Kauft das Buch und lädt sie neu, damit das Buch nicht mehr angezeigt wird
     * @param request
     * @param response
     * @return
     * @throws Exception
     */

    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        PlattformItem plattformItem = PlattformItemWebHelper.itemFromWeb(request);
        HashMap<String, Object> model = new HashMap<String, Object>();
        String idString = request.queryParams("id");
        int id = Integer.parseInt(idString);
        log.trace("POST /buy mit plattformDetail " + plattformItem);

        // Startet den Kaufprozess und lädt danach die Bücher neu
        model.put("info",repo.buy(id));
        model.put("list",repo.getAll());
        return new ModelAndView(model, "BookOverviewTemplate");
    }
}
