package ch.briggen.bfh.sparklist.web;

import ch.briggen.bfh.sparklist.domain.PlattformItem;
import ch.briggen.bfh.sparklist.domain.PlattformItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class BookCreateController implements TemplateViewRoute {

    private final Logger log = LoggerFactory.getLogger(BookCreateController.class);;
    private PlattformItemRepository repo = new PlattformItemRepository();

    /**
     * Startet den Insert-Prozess und leitet auf die Startseite weiter
     * @param request
     * @param response
     * @return
     * @throws Exception
     */

    @Override
    public ModelAndView handle(Request request, Response response) throws Exception {
        PlattformItem plattformItem = PlattformItemWebHelper.itemFromWeb(request);

        log.trace("POST /book/create mit plattformDetail " + plattformItem);

        //insert gibt die von der DB erstellte id zurück.
        int id = repo.insert(plattformItem);

        response.redirect("/");
        return null;
    }
}
