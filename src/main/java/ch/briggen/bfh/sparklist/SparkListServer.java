package ch.briggen.bfh.sparklist;

import static spark.Spark.get;
import static spark.Spark.post;

import java.util.Map.Entry;

import ch.briggen.bfh.sparklist.web.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.sparkbase.H2SparkApp;
import ch.briggen.sparkbase.UTF8ThymeleafTemplateEngine;
import spark.TemplateViewRoute;

public class SparkListServer extends H2SparkApp {

	final static Logger log = LoggerFactory.getLogger(SparkListServer.class);

	public static void main(String[] args) {
		
		for(Entry<Object, Object> property: System.getProperties().entrySet()) {
			log.debug(String.format("Property %s : %s", property.getKey(),property.getValue()));
		}

		SparkListServer server = new SparkListServer();
		server.configure();
		server.run();
	}

	/**
	 * Stellt die Routes mit den Controller zur Verfügung
	 */
	@Override
	protected void doConfigureHttpHandlers() {
		get("/", new BookManagementRootController(), new UTF8ThymeleafTemplateEngine());
		get("/detail", new BookDetailController(), new UTF8ThymeleafTemplateEngine());
		get("/search", new BookSearchController(), new UTF8ThymeleafTemplateEngine());
		post("/book/search", new BookSearchPlattformItemController(), new UTF8ThymeleafTemplateEngine());
		get("/book/new", new BookFormController(), new UTF8ThymeleafTemplateEngine());
		post("/book/create", new BookCreateController(), new UTF8ThymeleafTemplateEngine());
		get("/book", new BookUpdateController(), new UTF8ThymeleafTemplateEngine());
		post("/book/update", new BookUpdatePlattformItemController(), new UTF8ThymeleafTemplateEngine());
		get("/book/delete", new BookDeleteController(), new UTF8ThymeleafTemplateEngine());
		get("/buy", new BookBuyController(), new UTF8ThymeleafTemplateEngine());
		get("/impressum", new ImpressumController(), new UTF8ThymeleafTemplateEngine());
	}

}
