package ch.briggen.bfh.sparklist.domain;


/**
 * @author SmartBooks
 */
public class PlattformItem {

    private int itemID;
    private BenutzerItem benutzerItem = new BenutzerItem();
    private BuchItem buchItem = new BuchItem();
    private HochschulItem hsItem = new HochschulItem();
    private int verkauft;

    /**
     * Defaultkonstruktor für die Verwendung in einem Controller
     */
    public PlattformItem()
    {

    }

    /**
     * Konstruktor
     * @param itemID Eindeutige Id
     */
    public PlattformItem(int itemID, BenutzerItem benutzerItem, BuchItem buchItem, HochschulItem hsItem, int verkauft)
    {
        this.itemID = itemID;
        this.benutzerItem = benutzerItem;
        this.buchItem = buchItem;
        this.hsItem = hsItem;
        this.verkauft = verkauft;
    }

    public int getItemID() {
        return itemID;
    }

    public void setItemId(int itemID) {
        this.itemID = itemID;
    }

    public BenutzerItem getBenutzerItem() {
        return benutzerItem;
    }

    public void setBenutzerItem(BenutzerItem benutzerItem) {
        this.benutzerItem = benutzerItem;
    }

    public BuchItem getBuchItem() {
        return buchItem;
    }

    public void setBuchItem(BuchItem buchItem) {
        this.buchItem = buchItem;
    }

    public HochschulItem getHsItem() {
        return hsItem;
    }

    public void setHsItem(HochschulItem hsItem) {
         this.hsItem = hsItem;
    }

    public int getVerkauft() {
        return verkauft;
    }

    public void setVerkauft(int verkauft) {
        this.verkauft = verkauft;
    }

    @Override
    public String toString() {
        return String.format("Item:{id: %d; %s; %s; %s; verkauft: %d}", itemID, benutzerItem.toString(), buchItem.toString(), hsItem.toString(), verkauft);
    }

    /**
     * Überprüft ob ein PlattformItem identisch zu einem anderen ist.
     * @param pi
     * @return
     */
    public boolean isEqual(PlattformItem pi) {
        if(this.benutzerItem.isEqual(pi.benutzerItem)) {
            if(this.buchItem.isEqual(pi.buchItem)) {
                if(this.hsItem.isEqual(pi.hsItem)) {
                    if(this.verkauft == pi.verkauft) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

}
