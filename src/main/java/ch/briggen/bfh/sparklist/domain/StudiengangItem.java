package ch.briggen.bfh.sparklist.domain;


/**
 * @author SmartBooks
 */
public class StudiengangItem {

    private int sgID;
    private String sgName;


    /**
     * Defaultkonstruktor für die Verwendung in einem Controller
     */
    public StudiengangItem()
    {

    }

    /**
     * Konstruktor
     * @param sgID Eindeutige Id
     */
    public StudiengangItem(int sgID, String sgName)
    {
        this.sgID = sgID;
        this.sgName = sgName;
    }

    public int getSgId() {
        return sgID;
    }

    public void setSgId(int sgID){
        this.sgID =  sgID;

    }

    public String getSgName() {
        return sgName;
    }

    public void setSgName(String sgName) {
        this.sgName = sgName;
    }


    @Override
    public String toString() {
        return String.format("StudiengangItem:{sgID: %d; sgName: %s}", sgID, sgName);
    }

    /**
     * Überprüft ob ein StudiengangItem identisch zu einem anderen ist.
     * @param sg
     * @return
     */
    public boolean isEqual(StudiengangItem sg) {
        if(this.sgName.equals(sg.sgName)) {
            return true;
        }
        return false;
    }


}
