package ch.briggen.bfh.sparklist.domain;


/**
 * @author SmartBooks
 */
public class HochschulItem {

    private int hsID;
    private String hsName;
    private String abteilung;
    private String ort;
    private StudiengangItem sgItem = new StudiengangItem();


    /**
     * Defaultkonstruktor für die Verwendung in einem Controller
     */
    public HochschulItem()
    {

    }

    /**
     * Konstruktor
     * @param hsID Eindeutige Id
     */
    public HochschulItem(int hsID, String hsName, String abteilung, String ort, StudiengangItem sgItem)
    {
        this.hsID = hsID;
        this.hsName = hsName;
        this.abteilung = abteilung;
        this.ort = ort;
        this.sgItem = sgItem;
    }

    public int getHsId() {
        return hsID;
    }

    public void setHsId(int hsID) {
        this.hsID = hsID;
    }

    public String getHsName() {
        return hsName;
    }

    public void setHsName(String hsName) {
        this.hsName = hsName;
    }

    public String getAbteilung() {
        return abteilung;
    }

    public void setAbteilung(String abteilung) {
        this.abteilung = abteilung;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public StudiengangItem getSgItem() {
        return sgItem;
    }

    public void setSgItem(StudiengangItem sgItem) {
        this.sgItem = sgItem;
    }


    @Override
    public String toString() {
        return String.format("HochschulItem:{hsID: %d; hsName: %s; abteilung: %s; ort: %s; %s}", hsID, hsName, abteilung, ort, sgItem.toString());
    }

    /**
     * Überprüft ob HochschulItems identisch zu einem anderen ist
     * @param hs
     * @return
     */
    public boolean isEqual(HochschulItem hs) {
        if(this.hsName.equals(hs.hsName)) {
            if(this.abteilung.equals(hs.abteilung)) {
                if(this.ort.equals(hs.ort)) {
                    if(this.sgItem.isEqual(hs.sgItem)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

}
