package ch.briggen.bfh.sparklist.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

/**
 * @author SmartBooks
 */

public class PlattformItemRepository {
    private final Logger log = LoggerFactory.getLogger(PlattformItemRepository.class);


    /**
     * Liefert alle items in der Datenbank
     * @return Collection aller Items
     */
    public Collection<PlattformItem> getAll()  {
        log.trace("getAll");
        try(Connection conn = getConnection())
        {

            PreparedStatement stmt = conn.prepareStatement("SELECT PLATTFORMITEM.ITEM_ID, BENUTZER.BEN_ID, BENUTZER.NAME, BENUTZER.EMAIL, BUCH.B_ID ,BUCH.TITEL ,BUCH.ISBN ,BUCH.AUTHOR ,BUCH.VEROEFF_DAT, BUCH.VERSION, BUCH.PREIS , HOCHSCHULE.HS_ID, HOCHSCHULE.NAME, HOCHSCHULE.ABTEILUNG, HOCHSCHULE.ORT,STUDIENGANG.SG_ID, STUDIENGANG.NAME, PLATTFORMITEM.VERKAUFT\n" +
                    "FROM PLATTFORMITEM\n" +
                    "INNER JOIN BENUTZER ON BENUTZER.BEN_ID = PLATTFORMITEM.BEN_ID\n" +
                    "INNER JOIN BUCH ON BUCH.B_ID = PLATTFORMITEM.B_ID\n" +
                    "INNER JOIN HOCHSCHULE ON HOCHSCHULE.HS_ID = PLATTFORMITEM.HS_ID\n" +
                    "INNER JOIN STUDIENGANG ON STUDIENGANG.SG_ID = HOCHSCHULE.SG_ID WHERE PLATTFORMITEM.VERKAUFT = 0", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = stmt.executeQuery();

            if(rs.next()) {
                rs.beforeFirst();
                return mapItems(rs);
            } else {
                return new LinkedList<PlattformItem>();
            }


        }
        catch(SQLException e)
        {
            String msg = "SQL error while retreiving all items. ";
            log.error(msg, e);
            throw new RepositoryException(msg);
        }
    }

    /**
     * Liest die Hochschulen aus der Datenbank aus, um das Dropdown bei der Suche und auf der Startseite zu befüllen
     * @return ArrayList</String> Hochschulen
     */
    public ArrayList<String> populateDropdownHs(){
        log.trace("Populate DropDown");

        try(Connection conn = getConnection())
        {
            ArrayList <String> result = new ArrayList<String>();
            PreparedStatement stmtHS = conn.prepareStatement("SELECT HOCHSCHULE.NAME FROM HOCHSCHULE",
                    ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = stmtHS.executeQuery();
            ResultSetMetaData metaData = rs.getMetaData();

            int colNumber = metaData.getColumnCount();

            while(rs.next()){
                for(int i=1; i<=colNumber; i++){
                    result.add(rs.getString(i));
                }
            }
            return result;


        }
        catch(SQLException e)
        {
            String msg = "SQL error while retreiving all items. ";
            log.error(msg, e);
            throw new RepositoryException(msg);
        }
    }
    /**
     * Liest die Studiengänge aus der Datenbank aus, um das Dropdown bei der Suche und auf der Startseite zu befüllen
     * @return ArrayList</String> Studiengänge
     */
    public ArrayList<String> populateDropdownSg(){
        log.trace("Populate DropDown");

        try(Connection conn = getConnection())
        {
            ArrayList <String> result = new ArrayList<String>();
            PreparedStatement stmtHS = conn.prepareStatement("SELECT STUDIENGANG.NAME FROM STUDIENGANG",
                    ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = stmtHS.executeQuery();
            ResultSetMetaData metaData = rs.getMetaData();

            int colNumber = metaData.getColumnCount();

            while(rs.next()){
                for(int i=1; i<=colNumber; i++){

                    result.add(rs.getString(i));

                }
            }
            return result;


        }
        catch(SQLException e)
        {
            String msg = "SQL error while retreiving all items. ";
            log.error(msg, e);
            throw new RepositoryException(msg);
        }
    }

    /**
     * Liefert alle items in der Datenbank, welche die search-Kriterien erfüllen (ISBN, Titel, Hochschule, Studiengang)
     * @return Collection aller Items
     * @param i PlattformItem
     */
    public Collection<PlattformItem> search(PlattformItem i) {
        log.trace("search Items" + i);

        try(Connection conn = getConnection())
        {
            PreparedStatement searchStmt = conn.prepareStatement("");

            /** Überprüfen, welche Kriterien gewählt wurden und entsprechende Datenbankabfrage durchführen
             * Kriterium gilt als gewählt, wenn Daten != null und != "" (empty String)
             */
            if(i.getBuchItem().getTitel() != null & i.getBuchItem().getISBN() != null) {
                if (i.getBuchItem().getTitel().equals("") == false & (i.getBuchItem().getTitel() == null) == false) {
                    if (i.getBuchItem().getISBN().equals("") == false & i.getBuchItem().getISBN() == null) {
                        if (i.getHsItem().getHsName().equals("") == false) {
                            if (i.getHsItem().getSgItem().getSgName().equals("") == false) {
                                searchStmt = conn.prepareStatement("SELECT PLATTFORMITEM.ITEM_ID, BENUTZER.BEN_ID, BENUTZER.NAME, BENUTZER.EMAIL, BUCH.B_ID ,BUCH.TITEL ,BUCH.ISBN ,BUCH.AUTHOR ,BUCH.VEROEFF_DAT, BUCH.VERSION, BUCH.PREIS , HOCHSCHULE.HS_ID, HOCHSCHULE.NAME, HOCHSCHULE.ABTEILUNG, HOCHSCHULE.ORT,STUDIENGANG.SG_ID, STUDIENGANG.NAME, PLATTFORMITEM.VERKAUFT\n" +
                                        "FROM PLATTFORMITEM\n" +
                                        "INNER JOIN BENUTZER ON BENUTZER.BEN_ID = PLATTFORMITEM.BEN_ID\n" +
                                        "INNER JOIN BUCH ON BUCH.B_ID = PLATTFORMITEM.B_ID\n" +
                                        "INNER JOIN HOCHSCHULE ON HOCHSCHULE.HS_ID = PLATTFORMITEM.HS_ID\n" +
                                        "INNER JOIN STUDIENGANG ON STUDIENGANG.SG_ID = HOCHSCHULE.SG_ID WHERE BUCH.TITEL LIKE ? AND BUCH.ISBN LIKE ? AND HOCHSCHULE.NAME LIKE ? AND STUDIENGANG.NAME LIKE ? AND PLATTFORMITEM.VERKAUFT = 0", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                                searchStmt.setString(1, "%" + i.getBuchItem().getTitel().toUpperCase() + "%");
                                searchStmt.setString(2, "%" + i.getBuchItem().getISBN().toUpperCase() + "%");
                                searchStmt.setString(3, "%" + i.getHsItem().getHsName().toUpperCase() + "%");
                                searchStmt.setString(4, "%" + i.getHsItem().getSgItem().getSgName().toUpperCase() + "%");
                            } else {
                                searchStmt = conn.prepareStatement("SELECT PLATTFORMITEM.ITEM_ID, BENUTZER.BEN_ID, BENUTZER.NAME, BENUTZER.EMAIL, BUCH.B_ID ,BUCH.TITEL ,BUCH.ISBN ,BUCH.AUTHOR ,BUCH.VEROEFF_DAT, BUCH.VERSION, BUCH.PREIS , HOCHSCHULE.HS_ID, HOCHSCHULE.NAME, HOCHSCHULE.ABTEILUNG, HOCHSCHULE.ORT,STUDIENGANG.SG_ID, STUDIENGANG.NAME, PLATTFORMITEM.VERKAUFT\n" +
                                        "FROM PLATTFORMITEM\n" +
                                        "INNER JOIN BENUTZER ON BENUTZER.BEN_ID = PLATTFORMITEM.BEN_ID\n" +
                                        "INNER JOIN BUCH ON BUCH.B_ID = PLATTFORMITEM.B_ID\n" +
                                        "INNER JOIN HOCHSCHULE ON HOCHSCHULE.HS_ID = PLATTFORMITEM.HS_ID\n" +
                                        "INNER JOIN STUDIENGANG ON STUDIENGANG.SG_ID = HOCHSCHULE.SG_ID WHERE BUCH.TITEL LIKE ? AND BUCH.ISBN LIKE ? AND HOCHSCHULE.NAME LIKE ? AND PLATTFORMITEM.VERKAUFT = 0", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                                searchStmt.setString(1, "%" + i.getBuchItem().getTitel().toUpperCase() + "%");
                                searchStmt.setString(2, "%" + i.getBuchItem().getISBN().toUpperCase() + "%");
                                searchStmt.setString(3, "%" + i.getHsItem().getHsName().toUpperCase() + "%");
                            }
                        } else {
                            if (i.getHsItem().getSgItem().getSgName().equals("") == false) {
                                searchStmt = conn.prepareStatement("SELECT PLATTFORMITEM.ITEM_ID, BENUTZER.BEN_ID, BENUTZER.NAME, BENUTZER.EMAIL, BUCH.B_ID ,BUCH.TITEL ,BUCH.ISBN ,BUCH.AUTHOR ,BUCH.VEROEFF_DAT, BUCH.VERSION, BUCH.PREIS , HOCHSCHULE.HS_ID, HOCHSCHULE.NAME, HOCHSCHULE.ABTEILUNG, HOCHSCHULE.ORT,STUDIENGANG.SG_ID, STUDIENGANG.NAME, PLATTFORMITEM.VERKAUFT\n" +
                                        "FROM PLATTFORMITEM\n" +
                                        "INNER JOIN BENUTZER ON BENUTZER.BEN_ID = PLATTFORMITEM.BEN_ID\n" +
                                        "INNER JOIN BUCH ON BUCH.B_ID = PLATTFORMITEM.B_ID\n" +
                                        "INNER JOIN HOCHSCHULE ON HOCHSCHULE.HS_ID = PLATTFORMITEM.HS_ID\n" +
                                        "INNER JOIN STUDIENGANG ON STUDIENGANG.SG_ID = HOCHSCHULE.SG_ID WHERE BUCH.TITEL LIKE ? AND BUCH.ISBN LIKE ? AND STUDIENGANG.NAME LIKE ? AND PLATTFORMITEM.VERKAUFT = 0", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                                searchStmt.setString(1, "%" + i.getBuchItem().getTitel().toUpperCase() + "%");
                                searchStmt.setString(2, "%" + i.getBuchItem().getISBN().toUpperCase() + "%");
                                searchStmt.setString(3, "%" + i.getHsItem().getSgItem().getSgName().toUpperCase() + "%");
                            } else {
                                searchStmt = conn.prepareStatement("SELECT PLATTFORMITEM.ITEM_ID, BENUTZER.BEN_ID, BENUTZER.NAME, BENUTZER.EMAIL, BUCH.B_ID ,BUCH.TITEL ,BUCH.ISBN ,BUCH.AUTHOR ,BUCH.VEROEFF_DAT, BUCH.VERSION, BUCH.PREIS , HOCHSCHULE.HS_ID, HOCHSCHULE.NAME, HOCHSCHULE.ABTEILUNG, HOCHSCHULE.ORT,STUDIENGANG.SG_ID, STUDIENGANG.NAME, PLATTFORMITEM.VERKAUFT\n" +
                                        "FROM PLATTFORMITEM\n" +
                                        "INNER JOIN BENUTZER ON BENUTZER.BEN_ID = PLATTFORMITEM.BEN_ID\n" +
                                        "INNER JOIN BUCH ON BUCH.B_ID = PLATTFORMITEM.B_ID\n" +
                                        "INNER JOIN HOCHSCHULE ON HOCHSCHULE.HS_ID = PLATTFORMITEM.HS_ID\n" +
                                        "INNER JOIN STUDIENGANG ON STUDIENGANG.SG_ID = HOCHSCHULE.SG_ID WHERE BUCH.TITEL LIKE ? AND BUCH.ISBN LIKE ? AND PLATTFORMITEM.VERKAUFT = 0", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                                searchStmt.setString(1, "%" + i.getBuchItem().getTitel().toUpperCase() + "%");
                                searchStmt.setString(2, "%" + i.getBuchItem().getISBN().toUpperCase() + "%");
                            }
                        }
                    } else {
                        if (i.getHsItem().getHsName().equals("") == false) {
                            if (i.getHsItem().getSgItem().getSgName().equals("") == false) {
                                searchStmt = conn.prepareStatement("SELECT PLATTFORMITEM.ITEM_ID, BENUTZER.BEN_ID, BENUTZER.NAME, BENUTZER.EMAIL, BUCH.B_ID ,BUCH.TITEL ,BUCH.ISBN ,BUCH.AUTHOR ,BUCH.VEROEFF_DAT, BUCH.VERSION, BUCH.PREIS , HOCHSCHULE.HS_ID, HOCHSCHULE.NAME, HOCHSCHULE.ABTEILUNG, HOCHSCHULE.ORT,STUDIENGANG.SG_ID, STUDIENGANG.NAME, PLATTFORMITEM.VERKAUFT\n" +
                                        "FROM PLATTFORMITEM\n" +
                                        "INNER JOIN BENUTZER ON BENUTZER.BEN_ID = PLATTFORMITEM.BEN_ID\n" +
                                        "INNER JOIN BUCH ON BUCH.B_ID = PLATTFORMITEM.B_ID\n" +
                                        "INNER JOIN HOCHSCHULE ON HOCHSCHULE.HS_ID = PLATTFORMITEM.HS_ID\n" +
                                        "INNER JOIN STUDIENGANG ON STUDIENGANG.SG_ID = HOCHSCHULE.SG_ID WHERE BUCH.TITEL LIKE ? AND HOCHSCHULE.NAME LIKE ? AND STUDIENGANG.NAME LIKE ? AND PLATTFORMITEM.VERKAUFT = 0", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                                searchStmt.setString(1, "%" + i.getBuchItem().getTitel().toUpperCase() + "%");
                                searchStmt.setString(2, "%" + i.getHsItem().getHsName().toUpperCase() + "%");
                                searchStmt.setString(3, "%" + i.getHsItem().getSgItem().getSgName().toUpperCase() + "%");
                            } else {
                                searchStmt = conn.prepareStatement("SELECT PLATTFORMITEM.ITEM_ID, BENUTZER.BEN_ID, BENUTZER.NAME, BENUTZER.EMAIL, BUCH.B_ID ,BUCH.TITEL ,BUCH.ISBN ,BUCH.AUTHOR ,BUCH.VEROEFF_DAT, BUCH.VERSION, BUCH.PREIS , HOCHSCHULE.HS_ID, HOCHSCHULE.NAME, HOCHSCHULE.ABTEILUNG, HOCHSCHULE.ORT,STUDIENGANG.SG_ID, STUDIENGANG.NAME, PLATTFORMITEM.VERKAUFT\n" +
                                        "FROM PLATTFORMITEM\n" +
                                        "INNER JOIN BENUTZER ON BENUTZER.BEN_ID = PLATTFORMITEM.BEN_ID\n" +
                                        "INNER JOIN BUCH ON BUCH.B_ID = PLATTFORMITEM.B_ID\n" +
                                        "INNER JOIN HOCHSCHULE ON HOCHSCHULE.HS_ID = PLATTFORMITEM.HS_ID\n" +
                                        "INNER JOIN STUDIENGANG ON STUDIENGANG.SG_ID = HOCHSCHULE.SG_ID WHERE BUCH.TITEL LIKE ? AND HOCHSCHULE.NAME LIKE ? AND PLATTFORMITEM.VERKAUFT = 0", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                                searchStmt.setString(1, "%" + i.getBuchItem().getTitel().toUpperCase() + "%");
                                searchStmt.setString(2, "%" + i.getHsItem().getHsName().toUpperCase() + "%");
                            }
                        } else {
                            if (i.getHsItem().getSgItem().getSgName().equals("") == false) {
                                searchStmt = conn.prepareStatement("SELECT PLATTFORMITEM.ITEM_ID, BENUTZER.BEN_ID, BENUTZER.NAME, BENUTZER.EMAIL, BUCH.B_ID ,BUCH.TITEL ,BUCH.ISBN ,BUCH.AUTHOR ,BUCH.VEROEFF_DAT, BUCH.VERSION, BUCH.PREIS , HOCHSCHULE.HS_ID, HOCHSCHULE.NAME, HOCHSCHULE.ABTEILUNG, HOCHSCHULE.ORT,STUDIENGANG.SG_ID, STUDIENGANG.NAME, PLATTFORMITEM.VERKAUFT\n" +
                                        "FROM PLATTFORMITEM\n" +
                                        "INNER JOIN BENUTZER ON BENUTZER.BEN_ID = PLATTFORMITEM.BEN_ID\n" +
                                        "INNER JOIN BUCH ON BUCH.B_ID = PLATTFORMITEM.B_ID\n" +
                                        "INNER JOIN HOCHSCHULE ON HOCHSCHULE.HS_ID = PLATTFORMITEM.HS_ID\n" +
                                        "INNER JOIN STUDIENGANG ON STUDIENGANG.SG_ID = HOCHSCHULE.SG_ID WHERE BUCH.TITEL LIKE ? AND STUDIENGANG.NAME LIKE ? AND PLATTFORMITEM.VERKAUFT = 0", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                                searchStmt.setString(1, "%" + i.getBuchItem().getTitel().toUpperCase() + "%");
                                searchStmt.setString(2, "%" + i.getHsItem().getSgItem().getSgName().toUpperCase() + "%");
                            } else {
                                searchStmt = conn.prepareStatement("SELECT PLATTFORMITEM.ITEM_ID, BENUTZER.BEN_ID, BENUTZER.NAME, BENUTZER.EMAIL, BUCH.B_ID ,BUCH.TITEL ,BUCH.ISBN ,BUCH.AUTHOR ,BUCH.VEROEFF_DAT, BUCH.VERSION, BUCH.PREIS , HOCHSCHULE.HS_ID, HOCHSCHULE.NAME, HOCHSCHULE.ABTEILUNG, HOCHSCHULE.ORT,STUDIENGANG.SG_ID, STUDIENGANG.NAME, PLATTFORMITEM.VERKAUFT\n" +
                                        "FROM PLATTFORMITEM\n" +
                                        "INNER JOIN BENUTZER ON BENUTZER.BEN_ID = PLATTFORMITEM.BEN_ID\n" +
                                        "INNER JOIN BUCH ON BUCH.B_ID = PLATTFORMITEM.B_ID\n" +
                                        "INNER JOIN HOCHSCHULE ON HOCHSCHULE.HS_ID = PLATTFORMITEM.HS_ID\n" +
                                        "INNER JOIN STUDIENGANG ON STUDIENGANG.SG_ID = HOCHSCHULE.SG_ID WHERE BUCH.TITEL LIKE ? AND PLATTFORMITEM.VERKAUFT = 0", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                                searchStmt.setString(1, "%" + i.getBuchItem().getTitel().toUpperCase() + "%");
                            }
                        }
                    }
                } else {
                    if (i.getBuchItem().getISBN().equals("") == false & i.getBuchItem().getISBN() != null) {
                        if (i.getHsItem().getHsName().equals("") == false) {
                            if (i.getHsItem().getSgItem().getSgName().equals("") == false) {
                                searchStmt = conn.prepareStatement("SELECT PLATTFORMITEM.ITEM_ID, BENUTZER.BEN_ID, BENUTZER.NAME, BENUTZER.EMAIL, BUCH.B_ID ,BUCH.TITEL ,BUCH.ISBN ,BUCH.AUTHOR ,BUCH.VEROEFF_DAT, BUCH.VERSION, BUCH.PREIS , HOCHSCHULE.HS_ID, HOCHSCHULE.NAME, HOCHSCHULE.ABTEILUNG, HOCHSCHULE.ORT,STUDIENGANG.SG_ID, STUDIENGANG.NAME, PLATTFORMITEM.VERKAUFT\n" +
                                        "FROM PLATTFORMITEM\n" +
                                        "INNER JOIN BENUTZER ON BENUTZER.BEN_ID = PLATTFORMITEM.BEN_ID\n" +
                                        "INNER JOIN BUCH ON BUCH.B_ID = PLATTFORMITEM.B_ID\n" +
                                        "INNER JOIN HOCHSCHULE ON HOCHSCHULE.HS_ID = PLATTFORMITEM.HS_ID\n" +
                                        "INNER JOIN STUDIENGANG ON STUDIENGANG.SG_ID = HOCHSCHULE.SG_ID WHERE BUCH.ISBN LIKE ? AND HOCHSCHULE.NAME LIKE ? AND STUDIENGANG.NAME LIKE ? AND PLATTFORMITEM.VERKAUFT = 0", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                                searchStmt.setString(1, "%" + i.getBuchItem().getISBN().toUpperCase() + "%");
                                searchStmt.setString(2, "%" + i.getHsItem().getHsName().toUpperCase() + "%");
                                searchStmt.setString(3, "%" + i.getHsItem().getSgItem().getSgName().toUpperCase() + "%");
                            } else {
                                searchStmt = conn.prepareStatement("SELECT PLATTFORMITEM.ITEM_ID, BENUTZER.BEN_ID, BENUTZER.NAME, BENUTZER.EMAIL, BUCH.B_ID ,BUCH.TITEL ,BUCH.ISBN ,BUCH.AUTHOR ,BUCH.VEROEFF_DAT, BUCH.VERSION, BUCH.PREIS , HOCHSCHULE.HS_ID, HOCHSCHULE.NAME, HOCHSCHULE.ABTEILUNG, HOCHSCHULE.ORT,STUDIENGANG.SG_ID, STUDIENGANG.NAME, PLATTFORMITEM.VERKAUFT\n" +
                                        "FROM PLATTFORMITEM\n" +
                                        "INNER JOIN BENUTZER ON BENUTZER.BEN_ID = PLATTFORMITEM.BEN_ID\n" +
                                        "INNER JOIN BUCH ON BUCH.B_ID = PLATTFORMITEM.B_ID\n" +
                                        "INNER JOIN HOCHSCHULE ON HOCHSCHULE.HS_ID = PLATTFORMITEM.HS_ID\n" +
                                        "INNER JOIN STUDIENGANG ON STUDIENGANG.SG_ID = HOCHSCHULE.SG_ID WHERE BUCH.ISBN LIKE ? AND HOCHSCHULE.NAME LIKE ? AND PLATTFORMITEM.VERKAUFT = 0", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                                searchStmt.setString(1, "%" + i.getBuchItem().getISBN().toUpperCase() + "%");
                                searchStmt.setString(2, "%" + i.getHsItem().getHsName().toUpperCase() + "%");
                            }
                        } else {
                            if (i.getHsItem().getSgItem().getSgName().equals("") == false) {
                                searchStmt = conn.prepareStatement("SELECT PLATTFORMITEM.ITEM_ID, BENUTZER.BEN_ID, BENUTZER.NAME, BENUTZER.EMAIL, BUCH.B_ID ,BUCH.TITEL ,BUCH.ISBN ,BUCH.AUTHOR ,BUCH.VEROEFF_DAT, BUCH.VERSION, BUCH.PREIS , HOCHSCHULE.HS_ID, HOCHSCHULE.NAME, HOCHSCHULE.ABTEILUNG, HOCHSCHULE.ORT,STUDIENGANG.SG_ID, STUDIENGANG.NAME, PLATTFORMITEM.VERKAUFT\n" +
                                        "FROM PLATTFORMITEM\n" +
                                        "INNER JOIN BENUTZER ON BENUTZER.BEN_ID = PLATTFORMITEM.BEN_ID\n" +
                                        "INNER JOIN BUCH ON BUCH.B_ID = PLATTFORMITEM.B_ID\n" +
                                        "INNER JOIN HOCHSCHULE ON HOCHSCHULE.HS_ID = PLATTFORMITEM.HS_ID\n" +
                                        "INNER JOIN STUDIENGANG ON STUDIENGANG.SG_ID = HOCHSCHULE.SG_ID WHERE BUCH.ISBN LIKE ? AND STUDIENGANG.NAME LIKE ? AND PLATTFORMITEM.VERKAUFT = 0", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                                searchStmt.setString(1, "%" + i.getBuchItem().getISBN().toUpperCase() + "%");
                                searchStmt.setString(2, "%" + i.getHsItem().getSgItem().getSgName().toUpperCase() + "%");
                            } else {
                                searchStmt = conn.prepareStatement("SELECT PLATTFORMITEM.ITEM_ID, BENUTZER.BEN_ID, BENUTZER.NAME, BENUTZER.EMAIL, BUCH.B_ID ,BUCH.TITEL ,BUCH.ISBN ,BUCH.AUTHOR ,BUCH.VEROEFF_DAT, BUCH.VERSION, BUCH.PREIS , HOCHSCHULE.HS_ID, HOCHSCHULE.NAME, HOCHSCHULE.ABTEILUNG, HOCHSCHULE.ORT,STUDIENGANG.SG_ID, STUDIENGANG.NAME, PLATTFORMITEM.VERKAUFT\n" +
                                        "FROM PLATTFORMITEM\n" +
                                        "INNER JOIN BENUTZER ON BENUTZER.BEN_ID = PLATTFORMITEM.BEN_ID\n" +
                                        "INNER JOIN BUCH ON BUCH.B_ID = PLATTFORMITEM.B_ID\n" +
                                        "INNER JOIN HOCHSCHULE ON HOCHSCHULE.HS_ID = PLATTFORMITEM.HS_ID\n" +
                                        "INNER JOIN STUDIENGANG ON STUDIENGANG.SG_ID = HOCHSCHULE.SG_ID WHERE BUCH.ISBN LIKE ? AND PLATTFORMITEM.VERKAUFT = 0", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                                searchStmt.setString(1, "%" + i.getBuchItem().getISBN().toUpperCase() + "%");
                            }
                        }
                    } else {
                        if (i.getHsItem().getHsName().equals("") == false) {
                            if (i.getHsItem().getSgItem().getSgName().equals("") == false) {
                                searchStmt = conn.prepareStatement("SELECT PLATTFORMITEM.ITEM_ID, BENUTZER.BEN_ID, BENUTZER.NAME, BENUTZER.EMAIL, BUCH.B_ID ,BUCH.TITEL ,BUCH.ISBN ,BUCH.AUTHOR ,BUCH.VEROEFF_DAT, BUCH.VERSION, BUCH.PREIS , HOCHSCHULE.HS_ID, HOCHSCHULE.NAME, HOCHSCHULE.ABTEILUNG, HOCHSCHULE.ORT,STUDIENGANG.SG_ID, STUDIENGANG.NAME, PLATTFORMITEM.VERKAUFT\n" +
                                        "FROM PLATTFORMITEM\n" +
                                        "INNER JOIN BENUTZER ON BENUTZER.BEN_ID = PLATTFORMITEM.BEN_ID\n" +
                                        "INNER JOIN BUCH ON BUCH.B_ID = PLATTFORMITEM.B_ID\n" +
                                        "INNER JOIN HOCHSCHULE ON HOCHSCHULE.HS_ID = PLATTFORMITEM.HS_ID\n" +
                                        "INNER JOIN STUDIENGANG ON STUDIENGANG.SG_ID = HOCHSCHULE.SG_ID WHERE HOCHSCHULE.NAME LIKE ? AND STUDIENGANG.NAME LIKE ? AND PLATTFORMITEM.VERKAUFT = 0", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                                searchStmt.setString(1, "%" + i.getHsItem().getHsName().toUpperCase() + "%");
                                searchStmt.setString(2, "%" + i.getHsItem().getSgItem().getSgName().toUpperCase() + "%");
                            } else {
                                searchStmt = conn.prepareStatement("SELECT PLATTFORMITEM.ITEM_ID, BENUTZER.BEN_ID, BENUTZER.NAME, BENUTZER.EMAIL, BUCH.B_ID ,BUCH.TITEL ,BUCH.ISBN ,BUCH.AUTHOR ,BUCH.VEROEFF_DAT, BUCH.VERSION, BUCH.PREIS , HOCHSCHULE.HS_ID, HOCHSCHULE.NAME, HOCHSCHULE.ABTEILUNG, HOCHSCHULE.ORT,STUDIENGANG.SG_ID, STUDIENGANG.NAME, PLATTFORMITEM.VERKAUFT\n" +
                                        "FROM PLATTFORMITEM\n" +
                                        "INNER JOIN BENUTZER ON BENUTZER.BEN_ID = PLATTFORMITEM.BEN_ID\n" +
                                        "INNER JOIN BUCH ON BUCH.B_ID = PLATTFORMITEM.B_ID\n" +
                                        "INNER JOIN HOCHSCHULE ON HOCHSCHULE.HS_ID = PLATTFORMITEM.HS_ID\n" +
                                        "INNER JOIN STUDIENGANG ON STUDIENGANG.SG_ID = HOCHSCHULE.SG_ID WHERE HOCHSCHULE.NAME LIKE ? AND PLATTFORMITEM.VERKAUFT = 0", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                                searchStmt.setString(1, "%" + i.getHsItem().getHsName().toUpperCase() + "%");
                            }
                        } else {
                            if (i.getHsItem().getSgItem().getSgName().equals("") == false) {
                                searchStmt = conn.prepareStatement("SELECT PLATTFORMITEM.ITEM_ID, BENUTZER.BEN_ID, BENUTZER.NAME, BENUTZER.EMAIL, BUCH.B_ID ,BUCH.TITEL ,BUCH.ISBN ,BUCH.AUTHOR ,BUCH.VEROEFF_DAT, BUCH.VERSION, BUCH.PREIS , HOCHSCHULE.HS_ID, HOCHSCHULE.NAME, HOCHSCHULE.ABTEILUNG, HOCHSCHULE.ORT,STUDIENGANG.SG_ID, STUDIENGANG.NAME, PLATTFORMITEM.VERKAUFT\n" +
                                        "FROM PLATTFORMITEM\n" +
                                        "INNER JOIN BENUTZER ON BENUTZER.BEN_ID = PLATTFORMITEM.BEN_ID\n" +
                                        "INNER JOIN BUCH ON BUCH.B_ID = PLATTFORMITEM.B_ID\n" +
                                        "INNER JOIN HOCHSCHULE ON HOCHSCHULE.HS_ID = PLATTFORMITEM.HS_ID\n" +
                                        "INNER JOIN STUDIENGANG ON STUDIENGANG.SG_ID = HOCHSCHULE.SG_ID WHERE STUDIENGANG.NAME LIKE ? AND PLATTFORMITEM.VERKAUFT = 0", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                                searchStmt.setString(1, "%" + i.getHsItem().getSgItem().getSgName().toUpperCase() + "%");
                            } else {
                                searchStmt = conn.prepareStatement("SELECT PLATTFORMITEM.ITEM_ID, BENUTZER.BEN_ID, BENUTZER.NAME, BENUTZER.EMAIL, BUCH.B_ID ,BUCH.TITEL ,BUCH.ISBN ,BUCH.AUTHOR ,BUCH.VEROEFF_DAT, BUCH.VERSION, BUCH.PREIS , HOCHSCHULE.HS_ID, HOCHSCHULE.NAME, HOCHSCHULE.ABTEILUNG, HOCHSCHULE.ORT,STUDIENGANG.SG_ID, STUDIENGANG.NAME, PLATTFORMITEM.VERKAUFT\n" +
                                        "FROM PLATTFORMITEM\n" +
                                        "INNER JOIN BENUTZER ON BENUTZER.BEN_ID = PLATTFORMITEM.BEN_ID\n" +
                                        "INNER JOIN BUCH ON BUCH.B_ID = PLATTFORMITEM.B_ID\n" +
                                        "INNER JOIN HOCHSCHULE ON HOCHSCHULE.HS_ID = PLATTFORMITEM.HS_ID\n" +
                                        "INNER JOIN STUDIENGANG ON STUDIENGANG.SG_ID = HOCHSCHULE.SG_ID WHERE PLATTFORMITEM.VERKAUFT = 0", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                            }
                        }
                    }
                }
            } else {
                if (i.getHsItem().getHsName().equals("") == false) {
                    if (i.getHsItem().getSgItem().getSgName().equals("") == false) {
                        searchStmt = conn.prepareStatement("SELECT PLATTFORMITEM.ITEM_ID, BENUTZER.BEN_ID, BENUTZER.NAME, BENUTZER.EMAIL, BUCH.B_ID ,BUCH.TITEL ,BUCH.ISBN ,BUCH.AUTHOR ,BUCH.VEROEFF_DAT, BUCH.VERSION, BUCH.PREIS , HOCHSCHULE.HS_ID, HOCHSCHULE.NAME, HOCHSCHULE.ABTEILUNG, HOCHSCHULE.ORT,STUDIENGANG.SG_ID, STUDIENGANG.NAME, PLATTFORMITEM.VERKAUFT\n" +
                                "FROM PLATTFORMITEM\n" +
                                "INNER JOIN BENUTZER ON BENUTZER.BEN_ID = PLATTFORMITEM.BEN_ID\n" +
                                "INNER JOIN BUCH ON BUCH.B_ID = PLATTFORMITEM.B_ID\n" +
                                "INNER JOIN HOCHSCHULE ON HOCHSCHULE.HS_ID = PLATTFORMITEM.HS_ID\n" +
                                "INNER JOIN STUDIENGANG ON STUDIENGANG.SG_ID = HOCHSCHULE.SG_ID WHERE HOCHSCHULE.NAME LIKE ? AND STUDIENGANG.NAME LIKE ? AND PLATTFORMITEM.VERKAUFT = 0", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                        searchStmt.setString(1, "%" + i.getHsItem().getHsName().toUpperCase() + "%");
                        searchStmt.setString(2, "%" + i.getHsItem().getSgItem().getSgName().toUpperCase() + "%");
                    } else {
                        searchStmt = conn.prepareStatement("SELECT PLATTFORMITEM.ITEM_ID, BENUTZER.BEN_ID, BENUTZER.NAME, BENUTZER.EMAIL, BUCH.B_ID ,BUCH.TITEL ,BUCH.ISBN ,BUCH.AUTHOR ,BUCH.VEROEFF_DAT, BUCH.VERSION, BUCH.PREIS , HOCHSCHULE.HS_ID, HOCHSCHULE.NAME, HOCHSCHULE.ABTEILUNG, HOCHSCHULE.ORT,STUDIENGANG.SG_ID, STUDIENGANG.NAME, PLATTFORMITEM.VERKAUFT\n" +
                                "FROM PLATTFORMITEM\n" +
                                "INNER JOIN BENUTZER ON BENUTZER.BEN_ID = PLATTFORMITEM.BEN_ID\n" +
                                "INNER JOIN BUCH ON BUCH.B_ID = PLATTFORMITEM.B_ID\n" +
                                "INNER JOIN HOCHSCHULE ON HOCHSCHULE.HS_ID = PLATTFORMITEM.HS_ID\n" +
                                "INNER JOIN STUDIENGANG ON STUDIENGANG.SG_ID = HOCHSCHULE.SG_ID WHERE HOCHSCHULE.NAME LIKE ? AND PLATTFORMITEM.VERKAUFT = 0", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                        searchStmt.setString(1, "%" + i.getHsItem().getHsName().toUpperCase() + "%");
                    }
                } else {
                    if (i.getHsItem().getSgItem().getSgName().equals("") == false) {
                        searchStmt = conn.prepareStatement("SELECT PLATTFORMITEM.ITEM_ID, BENUTZER.BEN_ID, BENUTZER.NAME, BENUTZER.EMAIL, BUCH.B_ID ,BUCH.TITEL ,BUCH.ISBN ,BUCH.AUTHOR ,BUCH.VEROEFF_DAT, BUCH.VERSION, BUCH.PREIS , HOCHSCHULE.HS_ID, HOCHSCHULE.NAME, HOCHSCHULE.ABTEILUNG, HOCHSCHULE.ORT,STUDIENGANG.SG_ID, STUDIENGANG.NAME, PLATTFORMITEM.VERKAUFT\n" +
                                "FROM PLATTFORMITEM\n" +
                                "INNER JOIN BENUTZER ON BENUTZER.BEN_ID = PLATTFORMITEM.BEN_ID\n" +
                                "INNER JOIN BUCH ON BUCH.B_ID = PLATTFORMITEM.B_ID\n" +
                                "INNER JOIN HOCHSCHULE ON HOCHSCHULE.HS_ID = PLATTFORMITEM.HS_ID\n" +
                                "INNER JOIN STUDIENGANG ON STUDIENGANG.SG_ID = HOCHSCHULE.SG_ID WHERE STUDIENGANG.NAME LIKE ? AND PLATTFORMITEM.VERKAUFT = 0", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                        searchStmt.setString(1, "%" + i.getHsItem().getSgItem().getSgName().toUpperCase() + "%");
                    } else {
                        searchStmt = conn.prepareStatement("SELECT PLATTFORMITEM.ITEM_ID, BENUTZER.BEN_ID, BENUTZER.NAME, BENUTZER.EMAIL, BUCH.B_ID ,BUCH.TITEL ,BUCH.ISBN ,BUCH.AUTHOR ,BUCH.VEROEFF_DAT, BUCH.VERSION, BUCH.PREIS , HOCHSCHULE.HS_ID, HOCHSCHULE.NAME, HOCHSCHULE.ABTEILUNG, HOCHSCHULE.ORT,STUDIENGANG.SG_ID, STUDIENGANG.NAME, PLATTFORMITEM.VERKAUFT\n" +
                                "FROM PLATTFORMITEM\n" +
                                "INNER JOIN BENUTZER ON BENUTZER.BEN_ID = PLATTFORMITEM.BEN_ID\n" +
                                "INNER JOIN BUCH ON BUCH.B_ID = PLATTFORMITEM.B_ID\n" +
                                "INNER JOIN HOCHSCHULE ON HOCHSCHULE.HS_ID = PLATTFORMITEM.HS_ID\n" +
                                "INNER JOIN STUDIENGANG ON STUDIENGANG.SG_ID = HOCHSCHULE.SG_ID AND PLATTFORMITEM.VERKAUFT = 0", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                    }
                }
            }

            ResultSet rs = searchStmt.executeQuery();

            /** Prüfen, ob ResultSet leer ist, damit bei mapItems() keine exeption ausgelöst wird */
            if(rs.next()) {
                /** Reset des ResultSet-Cursor an den Start des ResultSet */
                rs.beforeFirst();
                return mapItems(rs);
            } else {
                return new LinkedList<PlattformItem>();
            }

        }
        catch(SQLException e)
        {
            String msg = "SQL error while retreiving searched items. ";
            log.error(msg, e);
            throw new RepositoryException(msg);
        }
    }

    /**
     * Gibt ein Buch anhand der Id zurück
     * @param id
     * @return PlattformItem
     */
    public PlattformItem getById(int id) {
        log.trace("getById " + id);

        //TODO: There is an issue with this repository method. Find and fix it!
        try(Connection conn = getConnection())
        {
            PreparedStatement stmt = conn.prepareStatement("SELECT PLATTFORMITEM.ITEM_ID, BENUTZER.BEN_ID, BENUTZER.NAME, BENUTZER.EMAIL, BUCH.B_ID ,BUCH.TITEL ,BUCH.ISBN ,BUCH.AUTHOR ,BUCH.VEROEFF_DAT, BUCH.VERSION, BUCH.PREIS , HOCHSCHULE.HS_ID, HOCHSCHULE.NAME, HOCHSCHULE.ABTEILUNG, HOCHSCHULE.ORT,STUDIENGANG.SG_ID, STUDIENGANG.NAME, PLATTFORMITEM.VERKAUFT\n" +
                    "FROM PLATTFORMITEM\n" +
                    "INNER JOIN BENUTZER ON BENUTZER.BEN_ID = PLATTFORMITEM.BEN_ID\n" +
                    "INNER JOIN BUCH ON BUCH.B_ID = PLATTFORMITEM.B_ID\n" +
                    "INNER JOIN HOCHSCHULE ON HOCHSCHULE.HS_ID = PLATTFORMITEM.HS_ID\n" +
                    "INNER JOIN STUDIENGANG ON STUDIENGANG.SG_ID = HOCHSCHULE.SG_ID WHERE ITEM_ID=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            return mapItems(rs).iterator().next();
        }
        catch(SQLException e)
        {
            String msg = "SQL error while retreiving items by id " + id;
            log.error(msg, e);
            throw new RepositoryException(msg);
        }

    }

    /**
     * Setzt den Verkauftwert auf 1 um zu zeigen, dass das Buch gekauft wurde
     * @param id
     * @return PlattformItem
     */
    public PlattformItem buy(int id){
        log.trace("buy " + id);

        int affectedRows;

        PreparedStatement stmtUpdate;

        try(Connection conn = getConnection())
        {
            //PlattformItem aktualisieren
            stmtUpdate = conn.prepareStatement("UPDATE PLATTFORMITEM SET VERKAUFT=? WHERE ITEM_ID=?", PreparedStatement.RETURN_GENERATED_KEYS);
            stmtUpdate.setInt(1,   1);
            stmtUpdate.setInt(2, id);

            PlattformItem returnItem = this.getById(id);


            affectedRows = stmtUpdate.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Buying Book failed, no rows affected.");
            }

            try (ResultSet generatedKeys = stmtUpdate.getGeneratedKeys()) {
                if (generatedKeys.next()) {

                } else {
                    throw new SQLException("Buying Book failed, no ID obtained.");
                }
            }

            return returnItem;

        }catch(Exception e)
        {
            String msg = "SQL error while updating item " + id;
            log.error(msg , e);
            throw new RepositoryException(msg);
        }
    }

    /**
     * Speichert neue Daten des übergebenen Items in der Datenbank. UPDATE.
     * @param i PlattformItem
     */
    public int update(PlattformItem i) {
        log.trace("save " + i);

        int affectedRows;
        int testForItems;
        int testForDependence;

        ResultSet checkForItems;

        PreparedStatement stmtCheckforItem;
        PreparedStatement stmtUpdate;
        PreparedStatement stmtGetId;
        PreparedStatement stmtDelete;

        try(Connection conn = getConnection())
        {

            // Studiengang aktualisieren
            boolean deleteOldStudiengangData = false;
            int oldStudiengangID = i.getHsItem().getSgItem().getSgId();

            //Testen, ob neue Daten bereits existieren
            stmtCheckforItem = conn.prepareStatement("SELECT COUNT(*) AS ROWCOUNT FROM STUDIENGANG WHERE NAME=?");
            stmtCheckforItem.setString(1, i.getHsItem().getSgItem().getSgName().toUpperCase());

            checkForItems = stmtCheckforItem.executeQuery();
            checkForItems.next();
            testForItems = checkForItems.getInt("ROWCOUNT");

            // Testen, ob mehrere Items auf dieses Item zugreiffen
            stmtCheckforItem = conn.prepareStatement("SELECT COUNT(*) AS ROWCOUNT FROM HOCHSCHULE WHERE SG_ID=?");
            stmtCheckforItem.setInt(1, i.getHsItem().getSgItem().getSgId());

            checkForItems = stmtCheckforItem.executeQuery();
            checkForItems.next();
            testForDependence = checkForItems.getInt("ROWCOUNT");

            stmtCheckforItem = conn.prepareStatement("SELECT COUNT(*) AS ROWCOUNT FROM PLATTFORMITEM WHERE HS_ID=?");
            stmtCheckforItem.setInt(1, i.getHsItem().getHsId());

            checkForItems = stmtCheckforItem.executeQuery();
            checkForItems.next();
            int testForDependence2 = checkForItems.getInt("ROWCOUNT");

            /** Abhängig von den Tests, wird PlattformItem mit existierendem Eintrag verlinkt,
             * neuer Eintrag wird erstellt oder der verwendete Eintrag wird verändert (update)
             */
            if(testForItems == 0) {

                if(testForDependence == 1 && testForDependence2 == 1) {
                    stmtUpdate = conn.prepareStatement("UPDATE STUDIENGANG SET NAME=? WHERE SG_ID=?", PreparedStatement.RETURN_GENERATED_KEYS);
                    stmtUpdate.setString(1, i.getHsItem().getSgItem().getSgName().toUpperCase());
                    stmtUpdate.setInt(2, i.getHsItem().getSgItem().getSgId());
                } else {
                    stmtUpdate = conn.prepareStatement("INSERT INTO STUDIENGANG(NAME) VALUES(?)", PreparedStatement.RETURN_GENERATED_KEYS);
                    stmtUpdate.setString(1, (i.getHsItem().getSgItem().getSgName()).toUpperCase());
                }

                affectedRows = stmtUpdate.executeUpdate();
                if (affectedRows == 0) {
                    throw new SQLException("Updating Studiengang failed, no rows affected.");
                }

                try (ResultSet generatedKeys = stmtUpdate.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        i.getHsItem().getSgItem().setSgId(generatedKeys.getInt(1));
                    } else {
                        throw new SQLException("Updating Studiengang failed, no ID obtained.");
                    }
                }

            } else {
                stmtGetId = conn.prepareStatement("SELECT SG_ID FROM STUDIENGANG WHERE NAME=?");
                stmtGetId.setString(1, i.getHsItem().getSgItem().getSgName().toUpperCase());

                ResultSet rs = stmtGetId.executeQuery();
                rs.next();
                i.getHsItem().getSgItem().setSgId(rs.getInt("SG_ID"));

                //alten Studiengang löschen?
                if(testForDependence == 1 && testForDependence2 == 1 && oldStudiengangID != i.getHsItem().getSgItem().getSgId()) {
                    deleteOldStudiengangData = true;
                }
            }


            // Hochschule aktualisieren
            boolean deleteOldHochschulData = false;
            int oldHochschulID = i.getHsItem().getHsId();

            //Testen, ob neue Daten bereits existieren
            stmtCheckforItem = conn.prepareStatement("SELECT COUNT(*) AS ROWCOUNT FROM HOCHSCHULE WHERE NAME=? AND ABTEILUNG=? AND ORT=? AND SG_ID=?");
            stmtCheckforItem.setString(1,i.getHsItem().getHsName().toUpperCase());
            stmtCheckforItem.setString(2,i.getHsItem().getAbteilung().toUpperCase());
            stmtCheckforItem.setString(3,i.getHsItem().getOrt().toUpperCase());
            stmtCheckforItem.setInt(4, i.getHsItem().getSgItem().getSgId());

            checkForItems = stmtCheckforItem.executeQuery();
            checkForItems.next();
            testForItems = checkForItems.getInt("ROWCOUNT");

            // Testen, ob mehrere Items auf dieses Item zugreiffen
            stmtCheckforItem = conn.prepareStatement("SELECT COUNT(*) AS ROWCOUNT FROM PLATTFORMITEM WHERE HS_ID=?");
            stmtCheckforItem.setInt(1, i.getHsItem().getHsId());

            checkForItems = stmtCheckforItem.executeQuery();
            checkForItems.next();
            testForDependence = checkForItems.getInt("ROWCOUNT");

            /** Abhängig von den Tests, wird PlattformItem mit existierendem Eintrag verlinkt,
             * neuer Eintrag wird erstellt oder der verwendete Eintrag wird verändert (update)
             */
            if(testForItems == 0) {

                if(testForDependence == 1) {
                    stmtUpdate = conn.prepareStatement("UPDATE HOCHSCHULE SET NAME=?, ABTEILUNG=?, ORT=? WHERE HS_ID=?", PreparedStatement.RETURN_GENERATED_KEYS);
                    stmtUpdate.setString(1, i.getHsItem().getHsName().toUpperCase());
                    stmtUpdate.setString(2, i.getHsItem().getAbteilung().toUpperCase());
                    stmtUpdate.setString(3, i.getHsItem().getOrt().toUpperCase());
                    stmtUpdate.setInt(4, i.getHsItem().getHsId());
                } else {
                    stmtUpdate = conn.prepareStatement("INSERT INTO HOCHSCHULE(NAME, ABTEILUNG, ORT, SG_ID) VALUES(?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
                    stmtUpdate.setString(1, i.getHsItem().getHsName().toUpperCase());
                    stmtUpdate.setString(2, i.getHsItem().getAbteilung().toUpperCase());
                    stmtUpdate.setString(3, i.getHsItem().getOrt().toUpperCase());
                    stmtUpdate.setInt(4, i.getHsItem().getSgItem().getSgId());
                }

                affectedRows = stmtUpdate.executeUpdate();
                if (affectedRows == 0) {
                    throw new SQLException("Updating Hochschule failed, no rows affected.");
                }

                try (ResultSet generatedKeys = stmtUpdate.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        i.getHsItem().setHsId(generatedKeys.getInt(1));
                    } else {
                        throw new SQLException("Updating Hochschule failed, no ID obtained.");
                    }
                }

            } else {
                stmtGetId = conn.prepareStatement("SELECT HS_ID FROM HOCHSCHULE WHERE NAME=? AND ABTEILUNG=? AND ORT=? AND SG_ID=?");
                stmtGetId.setString(1, i.getHsItem().getHsName().toUpperCase());
                stmtGetId.setString(2, i.getHsItem().getAbteilung().toUpperCase());
                stmtGetId.setString(3, i.getHsItem().getOrt().toUpperCase());
                stmtGetId.setInt(4, i.getHsItem().getSgItem().getSgId());

                ResultSet rs = stmtGetId.executeQuery();
                rs.next();
                i.getHsItem().setHsId(rs.getInt("HS_ID"));

                //alte Hochschule löschen?
                if(testForDependence == 1 && oldHochschulID != i.getHsItem().getHsId()) {
                    deleteOldHochschulData = true;
                }
            }


            //BUCH aktualisieren
            boolean deleteOldBuchData = false;
            int oldBuchID = i.getBuchItem().getBuchId();

            //Testen, ob neue Daten bereits existieren
            stmtCheckforItem = conn.prepareStatement("SELECT COUNT(*) AS ROWCOUNT FROM BUCH WHERE TITEL=? AND ISBN=? AND AUTHOR=? AND VEROEFF_DAT=? AND VERSION=? AND PREIS=?");
            stmtCheckforItem.setString(1, i.getBuchItem().getTitel().toUpperCase());
            stmtCheckforItem.setString(2, i.getBuchItem().getISBN().toUpperCase());
            stmtCheckforItem.setString(3, i.getBuchItem().getAuthor().toUpperCase());
            stmtCheckforItem.setDate(4, java.sql.Date.valueOf(i.getBuchItem().getVeroeff_dat()));
            stmtCheckforItem.setDouble(5, i.getBuchItem().getVersion());
            stmtCheckforItem.setDouble(6, i.getBuchItem().getPreis());

            checkForItems = stmtCheckforItem.executeQuery();
            checkForItems.next();
            testForItems = checkForItems.getInt("ROWCOUNT");

            // Testen, ob mehrere Items auf dieses Item zugreiffen
            stmtCheckforItem = conn.prepareStatement("SELECT COUNT(*) AS ROWCOUNT FROM PLATTFORMITEM WHERE B_ID=?");
            stmtCheckforItem.setInt(1, i.getBuchItem().getBuchId());

            checkForItems = stmtCheckforItem.executeQuery();
            checkForItems.next();
            testForDependence = checkForItems.getInt("ROWCOUNT");

            /** Abhängig von den Tests, wird PlattformItem mit existierendem Eintrag verlinkt,
             * neuer Eintrag wird erstellt oder der verwendete Eintrag wird verändert (update)
             */
            if(testForItems == 0) {

                if(testForDependence == 1) {
                    stmtUpdate = conn.prepareStatement("UPDATE BUCH SET TITEL=?, ISBN=?, AUTHOR=?, VEROEFF_DAT=?, VERSION=?, PREIS=? WHERE B_ID=?", PreparedStatement.RETURN_GENERATED_KEYS);
                    stmtUpdate.setString(1, i.getBuchItem().getTitel().toUpperCase());
                    stmtUpdate.setString(2, i.getBuchItem().getISBN().toUpperCase());
                    stmtUpdate.setString(3, i.getBuchItem().getAuthor().toUpperCase());
                    stmtUpdate.setDate(4, java.sql.Date.valueOf(i.getBuchItem().getVeroeff_dat()));
                    stmtUpdate.setDouble(5, i.getBuchItem().getVersion());
                    stmtUpdate.setDouble(6, i.getBuchItem().getPreis());
                    stmtUpdate.setInt(7, i.getBuchItem().getBuchId());
                } else {
                    stmtUpdate = conn.prepareStatement("INSERT INTO BUCH(TITEL, ISBN, AUTHOR, VEROEFF_DAT, VERSION, PREIS) VALUES(?,?,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
                    stmtUpdate.setString(1, i.getBuchItem().getTitel().toUpperCase());
                    stmtUpdate.setString(2, i.getBuchItem().getISBN().toUpperCase());
                    stmtUpdate.setString(3, i.getBuchItem().getAuthor().toUpperCase());
                    stmtUpdate.setDate(4, java.sql.Date.valueOf(i.getBuchItem().getVeroeff_dat()));
                    stmtUpdate.setDouble(5, i.getBuchItem().getVersion());
                    stmtUpdate.setDouble(6, i.getBuchItem().getPreis());
                }

                affectedRows = stmtUpdate.executeUpdate();
                if (affectedRows == 0) {
                    throw new SQLException("Updating Buch failed, no rows affected.");
                }

                try (ResultSet generatedKeys = stmtUpdate.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        i.getBuchItem().setBuchId(generatedKeys.getInt(1));
                    } else {
                        throw new SQLException("Updating Buch failed, no ID obtained.");
                    }
                }

            } else {
                stmtGetId = conn.prepareStatement("SELECT B_ID FROM BUCH WHERE TITEL=? AND ISBN=? AND AUTHOR=? AND VEROEFF_DAT=? AND VERSION=? AND PREIS=?");
                stmtGetId.setString(1, i.getBuchItem().getTitel().toUpperCase());
                stmtGetId.setString(2, i.getBuchItem().getISBN().toUpperCase());
                stmtGetId.setString(3, i.getBuchItem().getAuthor().toUpperCase());
                stmtGetId.setDate(4, java.sql.Date.valueOf(i.getBuchItem().getVeroeff_dat()));
                stmtGetId.setDouble(5, i.getBuchItem().getVersion());
                stmtGetId.setDouble(6, i.getBuchItem().getPreis());

                ResultSet rs = stmtGetId.executeQuery();
                rs.next();
                i.getBuchItem().setBuchId(rs.getInt("B_ID"));

                //altes Buch löschen?
                if(testForDependence == 1 && oldBuchID != i.getBuchItem().getBuchId()) {
                    deleteOldBuchData = true;
                }
            }

            //Benutzer aktualisieren
            boolean deleteOldBenutzerData = false;
            int oldBenutzerID = i.getBenutzerItem().getBenutzerId();

            //Testen, ob neue Daten bereits existieren
            stmtCheckforItem = conn.prepareStatement("SELECT COUNT(*) AS ROWCOUNT FROM BENUTZER WHERE NAME=? AND EMAIL=?");
            stmtCheckforItem.setString(1,i.getBenutzerItem().getBenutzerName().toUpperCase());
            stmtCheckforItem.setString(2, i.getBenutzerItem().getEmail().toLowerCase());

            checkForItems = stmtCheckforItem.executeQuery();
            checkForItems.next();
            testForItems = checkForItems.getInt("ROWCOUNT");

            // Testen, ob mehrere Items auf dieses Item zugreiffen
            stmtCheckforItem = conn.prepareStatement("SELECT COUNT(*) AS ROWCOUNT FROM PLATTFORMITEM WHERE BEN_ID=?");
            stmtCheckforItem.setInt(1, i.getBenutzerItem().getBenutzerId());

            checkForItems = stmtCheckforItem.executeQuery();
            checkForItems.next();
            testForDependence = checkForItems.getInt("ROWCOUNT");

            /** Abhängig von den Tests, wird PlattformItem mit existierendem Eintrag verlinkt,
             * neuer Eintrag wird erstellt oder der verwendete Eintrag wird verändert (update)
             */
            if(testForItems == 0) {

                if(testForDependence == 1) {
                    stmtUpdate = conn.prepareStatement("UPDATE BENUTZER SET NAME=?, EMAIL=? WHERE BEN_ID=?", PreparedStatement.RETURN_GENERATED_KEYS);
                    stmtUpdate.setString(1, i.getBenutzerItem().getBenutzerName().toUpperCase());
                    stmtUpdate.setString(2, i.getBenutzerItem().getEmail().toLowerCase());
                    stmtUpdate.setInt(3, i.getBenutzerItem().getBenutzerId());
                } else {
                    stmtUpdate = conn.prepareStatement("INSERT INTO BENUTZER(NAME,EMAIL) VALUES (?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
                    stmtUpdate.setString(1, i.getBenutzerItem().getBenutzerName().toUpperCase());
                    stmtUpdate.setString(2, i.getBenutzerItem().getEmail().toLowerCase());
                }

                affectedRows = stmtUpdate.executeUpdate();
                if (affectedRows == 0) {
                    throw new SQLException("Updating Benutzer failed, no rows affected.");
                }

                try (ResultSet generatedKeys = stmtUpdate.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        i.getBenutzerItem().setBenutzerId(generatedKeys.getInt(1));
                    } else {
                        throw new SQLException("Updating Benutzer failed, no ID obtained.");
                    }
                }

            } else {
                stmtGetId = conn.prepareStatement("SELECT BEN_ID FROM BENUTZER WHERE NAME=? AND EMAIL=?");
                stmtGetId.setString(1, i.getBenutzerItem().getBenutzerName().toUpperCase());
                stmtGetId.setString(2, i.getBenutzerItem().getEmail().toLowerCase());

                ResultSet rs = stmtGetId.executeQuery();
                rs.next();
                i.getBenutzerItem().setBenutzerId(rs.getInt("BEN_ID"));

                //alten Benutzer löschen?
                if(testForDependence == 1 && oldBenutzerID != i.getBenutzerItem().getBenutzerId()) {
                    deleteOldBenutzerData = true;
                }
            }


            //PlattformItem aktualisieren
            stmtUpdate = conn.prepareStatement("UPDATE PLATTFORMITEM SET BEN_ID=?, B_ID=?, HS_ID=?, VERKAUFT=? WHERE ITEM_ID=?", PreparedStatement.RETURN_GENERATED_KEYS);
            stmtUpdate.setInt(1, i.getBenutzerItem().getBenutzerId());
            stmtUpdate.setInt(2, i.getBuchItem().getBuchId());
            stmtUpdate.setInt(3, i.getHsItem().getHsId());
            stmtUpdate.setInt(4, i.getVerkauft());
            stmtUpdate.setInt(5, i.getItemID());

            affectedRows = stmtUpdate.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Updating PlattformItem failed, no rows affected.");
            }

            try (ResultSet generatedKeys = stmtUpdate.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    i.setItemId(generatedKeys.getInt(1));
                } else {
                    throw new SQLException("Updating PlattformItem failed, no ID obtained.");
                }
            }

            // Löschen von nicht mehr referenzierter Daten
            if(deleteOldHochschulData) {
                stmtDelete = conn.prepareStatement("DELETE FROM HOCHSCHULE WHERE HS_ID=?");
                stmtDelete.setInt(1, oldHochschulID);
                stmtDelete.executeUpdate();
                resetHochschulTabelle();
            }

            if(deleteOldStudiengangData) {
                stmtDelete = conn.prepareStatement("DELETE FROM STUDIENGANG WHERE SG_ID=?");
                stmtDelete.setInt(1, oldStudiengangID);
                stmtDelete.executeUpdate();
                resetStudiengangTabelle();
            }

            if(deleteOldBuchData) {
                stmtDelete = conn.prepareStatement("DELETE FROM BUCH WHERE B_ID=?");
                stmtDelete.setInt(1, oldBuchID);
                stmtDelete.executeUpdate();
                resetBuchTabelle();
            }

            if(deleteOldBenutzerData) {
                stmtDelete = conn.prepareStatement("DELETE FROM BENUTZER WHERE BEN_ID=?");
                stmtDelete.setInt(1, oldBenutzerID);
                stmtDelete.executeUpdate();
                resetBenutzerTabelle();
            }


            log.trace(i.toString());
            return i.getItemID();
        }
        catch(Exception e)
        {
            String msg = "SQL error while updating item " + i;
            log.error(msg , e);
            throw new RepositoryException(msg);
        }

    }

    /**
     * Löscht das Item mit der angegebenen Id von der DB
     * @param id Item ID
     */
    public void delete(int id) {
        log.trace("delete " + id);
        PlattformItem i = getById(id);

        PreparedStatement stmtDelete;
        PreparedStatement stmtCheckforItem;

        ResultSet checkForItems;

        try(Connection conn = getConnection())
        {
            //Löschen des PlattformItems
            stmtDelete = conn.prepareStatement("DELETE FROM PLATTFORMITEM WHERE ITEM_ID=?");
            stmtDelete.setInt(1, i.getItemID());
            stmtDelete.executeUpdate();
            resetPlattformItemTabelle();

            // Benutzer löschen?
            // Testen, ob ein anderes PlattformItem auf dieses BenutzerItem zugreifft
            stmtCheckforItem = conn.prepareStatement("SELECT COUNT(*) AS ROWCOUNT FROM PLATTFORMITEM WHERE BEN_ID=?");
            stmtCheckforItem.setInt(1, i.getBenutzerItem().getBenutzerId());

            checkForItems = stmtCheckforItem.executeQuery();
            checkForItems.next();

            // Löschen des BenutzerItems
            if(checkForItems.getInt("ROWCOUNT") == 0) {
                stmtDelete = conn.prepareStatement("DELETE FROM BENUTZER WHERE BEN_ID=?");
                stmtDelete.setInt(1, i.getBenutzerItem().getBenutzerId());
                stmtDelete.executeUpdate();
                resetBenutzerTabelle();
            }

            // Buch löschen?
            // Testen, ob ein PlattformItem auf dieses BuchItem zugreifft
            stmtCheckforItem = conn.prepareStatement("SELECT COUNT(*) AS ROWCOUNT FROM PLATTFORMITEM WHERE B_ID=?");
            stmtCheckforItem.setInt(1, i.getBuchItem().getBuchId());

            checkForItems = stmtCheckforItem.executeQuery();
            checkForItems.next();

            // Löschen des BuchItems
            if(checkForItems.getInt("ROWCOUNT") == 0) {
                stmtDelete = conn.prepareStatement("DELETE FROM BUCH WHERE B_ID=?");
                stmtDelete.setInt(1, i.getBuchItem().getBuchId());
                stmtDelete.executeUpdate();
                resetBuchTabelle();
            }

            // Hochschule löschen?
            // Testen, ob ein PlattformItem auf dieses HochschulItem zugreifft
            stmtCheckforItem = conn.prepareStatement("SELECT COUNT(*) AS ROWCOUNT FROM PLATTFORMITEM WHERE HS_ID=?");
            stmtCheckforItem.setInt(1, i.getHsItem().getHsId());

            checkForItems = stmtCheckforItem.executeQuery();
            checkForItems.next();

            // HochschulItem löschen
            if(checkForItems.getInt("ROWCOUNT") == 0) {
                stmtDelete = conn.prepareStatement("DELETE FROM HOCHSCHULE WHERE HS_ID=?");
                stmtDelete.setInt(1, i.getHsItem().getHsId());
                stmtDelete.executeUpdate();
                resetHochschulTabelle();
            }

            //Studiengang löschen?
            // Testen, ob ein HochschulItem auf dieses StudiengangItem zugreifft
            stmtCheckforItem = conn.prepareStatement("SELECT COUNT(*) AS ROWCOUNT FROM HOCHSCHULE WHERE SG_ID=?");
            stmtCheckforItem.setInt(1, i.getHsItem().getSgItem().getSgId());

            checkForItems = stmtCheckforItem.executeQuery();
            checkForItems.next();

            // StudiengangItem löschen
            if(checkForItems.getInt("ROWCOUNT") == 0) {
                stmtDelete = conn.prepareStatement("DELETE FROM STUDIENGANG WHERE SG_ID=?");
                stmtDelete.setInt(1, i.getHsItem().getSgItem().getSgId());
                stmtDelete.executeUpdate();
                resetStudiengangTabelle();
            }

        }
        catch(SQLException e)
        {
            String msg = "SQL error while deleting items by id " + id;
            log.error(msg, e);
            throw new RepositoryException(msg);
        }


    }

    /**
     * Speichert das angegebene Item in der DB. INSERT.
     * @param i neu zu erstellendes Item
     * @return Liefert die von der DB generierte id des neuen Items zurück
     */
    public int insert(PlattformItem i) {

        log.trace("insert " + i);
        int affectedRows;
        ResultSet checkForItems;

        PreparedStatement stmtCheckforItem;
        PreparedStatement stmtInsert;
        PreparedStatement stmtGetId;

        try(Connection conn = getConnection())
        {

            // Studiengang hinzufügen
            stmtCheckforItem = conn.prepareStatement("SELECT COUNT(*) AS ROWCOUNT FROM STUDIENGANG WHERE NAME=?");
            stmtCheckforItem.setString(1, i.getHsItem().getSgItem().getSgName().toUpperCase());

            checkForItems = stmtCheckforItem.executeQuery();
            checkForItems.next();

            /** Prüfen, ob Item bereits existiert
             *  existiert Item -> verlinken mit dem existierenden Item
             *  existiert Item nicht -> neues Item erstellen
             */
            if(checkForItems.getInt("ROWCOUNT") == 0) {

                stmtInsert = conn.prepareStatement("INSERT INTO STUDIENGANG(NAME) VALUES(?)", PreparedStatement.RETURN_GENERATED_KEYS);
                stmtInsert.setString(1, (i.getHsItem().getSgItem().getSgName()).toUpperCase());

                affectedRows = stmtInsert.executeUpdate();
                if (affectedRows == 0) {
                    throw new SQLException("Creating Studiengang failed, no rows affected.");
                }

                try (ResultSet generatedKeys = stmtInsert.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        i.getHsItem().getSgItem().setSgId(generatedKeys.getInt(1));
                    } else {
                        throw new SQLException("Creating Studiengang failed, no ID obtained.");
                    }
                }

            } else {
                stmtGetId = conn.prepareStatement("SELECT SG_ID FROM STUDIENGANG WHERE NAME=?");
                stmtGetId.setString(1, i.getHsItem().getSgItem().getSgName().toUpperCase());

                ResultSet rs = stmtGetId.executeQuery();
                rs.next();
                i.getHsItem().getSgItem().setSgId(rs.getInt("SG_ID"));
            }


            // Hochschule hinzufügen
            stmtCheckforItem = conn.prepareStatement("SELECT COUNT(*) AS ROWCOUNT FROM HOCHSCHULE WHERE NAME=? AND ABTEILUNG=? AND ORT=?");
            stmtCheckforItem.setString(1,i.getHsItem().getHsName().toUpperCase());
            stmtCheckforItem.setString(2,i.getHsItem().getAbteilung().toUpperCase());
            stmtCheckforItem.setString(3,i.getHsItem().getOrt().toUpperCase());

            checkForItems = stmtCheckforItem.executeQuery();
            checkForItems.next();

            /** Prüfen, ob Item bereits existiert
             *  existiert Item -> verlinken mit dem existierenden Item
             *  existiert Item nicht -> neues Item erstellen
             */
            if(checkForItems.getInt("ROWCOUNT") == 0) {

                stmtInsert = conn.prepareStatement("INSERT INTO HOCHSCHULE(NAME, ABTEILUNG, ORT, SG_ID) VALUES(?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
                stmtInsert.setString(1, i.getHsItem().getHsName().toUpperCase());
                stmtInsert.setString(2, i.getHsItem().getAbteilung().toUpperCase());
                stmtInsert.setString(3, i.getHsItem().getOrt().toUpperCase());
                stmtInsert.setInt(4, i.getHsItem().getSgItem().getSgId());

                affectedRows = stmtInsert.executeUpdate();
                if (affectedRows == 0) {
                    throw new SQLException("Creating Hochschule failed, no rows affected.");
                }

                try (ResultSet generatedKeys = stmtInsert.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        i.getHsItem().setHsId(generatedKeys.getInt(1));
                    } else {
                        throw new SQLException("Creating Hochschule failed, no ID obtained.");
                    }
                }

            } else {
                stmtGetId = conn.prepareStatement("SELECT HS_ID FROM HOCHSCHULE WHERE NAME=? AND ABTEILUNG=? AND ORT=?");
                stmtGetId.setString(1, i.getHsItem().getHsName().toUpperCase());
                stmtGetId.setString(2, i.getHsItem().getAbteilung().toUpperCase());
                stmtGetId.setString(3, i.getHsItem().getOrt().toUpperCase());

                ResultSet rs = stmtGetId.executeQuery();
                rs.next();
                i.getHsItem().setHsId(rs.getInt("HS_ID"));
            }


            // Buch hinzufügen
            stmtCheckforItem = conn.prepareStatement("SELECT COUNT(*) AS ROWCOUNT FROM BUCH WHERE TITEL=? AND ISBN=? AND AUTHOR=? AND VEROEFF_DAT=? AND VERSION=? AND PREIS=?");
            stmtCheckforItem.setString(1, i.getBuchItem().getTitel().toUpperCase());
            stmtCheckforItem.setString(2, i.getBuchItem().getISBN().toUpperCase());
            stmtCheckforItem.setString(3, i.getBuchItem().getAuthor().toUpperCase());
            stmtCheckforItem.setDate(4, java.sql.Date.valueOf(i.getBuchItem().getVeroeff_dat()));
            stmtCheckforItem.setDouble(5, i.getBuchItem().getVersion());
            stmtCheckforItem.setDouble(6, i.getBuchItem().getPreis());

            checkForItems = stmtCheckforItem.executeQuery();
            checkForItems.next();

            /** Prüfen, ob Item bereits existiert
             *  existiert Item -> verlinken mit dem existierenden Item
             *  existiert Item nicht -> neues Item erstellen
             */
            if(checkForItems.getInt("ROWCOUNT") == 0) {

                stmtInsert = conn.prepareStatement("INSERT INTO BUCH(TITEL, ISBN, AUTHOR, VEROEFF_DAT, VERSION, PREIS) VALUES(?,?,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
                stmtInsert.setString(1, i.getBuchItem().getTitel().toUpperCase());
                stmtInsert.setString(2, i.getBuchItem().getISBN().toUpperCase());
                stmtInsert.setString(3, i.getBuchItem().getAuthor().toUpperCase());
                stmtInsert.setDate(4, java.sql.Date.valueOf(i.getBuchItem().getVeroeff_dat()));
                stmtInsert.setDouble(5, i.getBuchItem().getVersion());
                stmtInsert.setDouble(6, i.getBuchItem().getPreis());

                affectedRows = stmtInsert.executeUpdate();
                if (affectedRows == 0) {
                    throw new SQLException("Creating Buch failed, no rows affected.");
                }

                try (ResultSet generatedKeys = stmtInsert.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        i.getBuchItem().setBuchId(generatedKeys.getInt(1));
                    } else {
                        throw new SQLException("Creating Buch failed, no ID obtained.");
                    }
                }

            } else {
                stmtGetId = conn.prepareStatement("SELECT B_ID FROM BUCH WHERE TITEL=? AND ISBN=? AND AUTHOR=? AND VEROEFF_DAT=? AND VERSION=? AND PREIS=?");
                stmtGetId.setString(1, i.getBuchItem().getTitel().toUpperCase());
                stmtGetId.setString(2, i.getBuchItem().getISBN().toUpperCase());
                stmtGetId.setString(3, i.getBuchItem().getAuthor().toUpperCase());
                stmtGetId.setDate(4, java.sql.Date.valueOf(i.getBuchItem().getVeroeff_dat()));
                stmtGetId.setDouble(5, i.getBuchItem().getVersion());
                stmtGetId.setDouble(6, i.getBuchItem().getPreis());

                ResultSet rs = stmtGetId.executeQuery();
                rs.next();
                i.getBuchItem().setBuchId(rs.getInt("B_ID"));
            }


            //Benutzer hinzufügen
            stmtCheckforItem = conn.prepareStatement("SELECT COUNT(*) AS ROWCOUNT FROM BENUTZER WHERE NAME=? AND EMAIL=?");
            stmtCheckforItem.setString(1,i.getBenutzerItem().getBenutzerName().toUpperCase());
            stmtCheckforItem.setString(2, i.getBenutzerItem().getEmail().toLowerCase());

            checkForItems = stmtCheckforItem.executeQuery();
            checkForItems.next();

            /** Prüfen, ob Item bereits existiert
             *  existiert Item -> verlinken mit dem existierenden Item
             *  existiert Item nicht -> neues Item erstellen
             */
            if(checkForItems.getInt("ROWCOUNT") == 0) {

                stmtInsert = conn.prepareStatement("INSERT INTO BENUTZER(NAME,EMAIL) VALUES (?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
                stmtInsert.setString(1, i.getBenutzerItem().getBenutzerName().toUpperCase());
                stmtInsert.setString(2, i.getBenutzerItem().getEmail().toLowerCase());

                affectedRows = stmtInsert.executeUpdate();
                if (affectedRows == 0) {
                    throw new SQLException("Creating Benutzer failed, no rows affected.");
                }

                try (ResultSet generatedKeys = stmtInsert.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        i.getBenutzerItem().setBenutzerId(generatedKeys.getInt(1));
                    } else {
                        throw new SQLException("Creating Benutzer failed, no ID obtained.");
                    }
                }

            } else {
                stmtGetId = conn.prepareStatement("SELECT BEN_ID FROM BENUTZER WHERE NAME=? AND EMAIL=?");
                stmtGetId.setString(1, i.getBenutzerItem().getBenutzerName().toUpperCase());
                stmtGetId.setString(2, i.getBenutzerItem().getEmail().toLowerCase());

                ResultSet rs = stmtGetId.executeQuery();
                rs.next();
                i.getBenutzerItem().setBenutzerId(rs.getInt("BEN_ID"));
            }


            // PlattformItem hinzufügen
            // Nicht überprüfen, ob Item bereits existiert, da meherere gleiche Items existieren dürfen
            stmtInsert = conn.prepareStatement("INSERT INTO PLATTFORMITEM(BEN_ID, B_ID, HS_ID, VERKAUFT) VALUES(?, ?, ?, ?)", PreparedStatement.RETURN_GENERATED_KEYS);
            stmtInsert.setInt(1, i.getBenutzerItem().getBenutzerId());
            stmtInsert.setInt(2, i.getBuchItem().getBuchId());
            stmtInsert.setInt(3, i.getHsItem().getHsId());
            stmtInsert.setInt(4, i.getVerkauft());

            affectedRows = stmtInsert.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creating PlattformItem failed, no rows affected.");
            }

            try (ResultSet generatedKeys = stmtInsert.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    i.setItemId(generatedKeys.getInt(1));
                } else {
                    throw new SQLException("Creating PlattformItem failed, no ID obtained.");
                }
            }

            return i.getItemID();
        }
        catch(SQLException e)
        {
            String msg = "SQL error while inserting item " + i;
            log.error(msg , e);
            throw new RepositoryException(msg);
        }

    }

    /**
     * Helper zum konvertieren der Resultsets in Item-Objekte. Siehe getByXXX Methoden.
     * @author SmartBooks
     * @throws SQLException
     *
     **/
    private static Collection<PlattformItem> mapItems(ResultSet rs) throws SQLException
    {
        LinkedList<PlattformItem> list = new LinkedList<PlattformItem>();
        while(rs.next())
        {
            BenutzerItem bn = new BenutzerItem(rs.getInt("BENUTZER.BEN_ID"),rs.getString("BENUTZER.EMAIL"),rs.getString("BENUTZER.NAME"));
            BuchItem bi = new BuchItem(rs.getInt("BUCH.B_ID"),rs.getString("BUCH.TITEL"),rs.getString("BUCH.ISBN"),rs.getString("BUCH.AUTHOR"),rs.getDate("BUCH.VEROEFF_DAT").toLocalDate(),rs.getDouble("BUCH.VERSION"),rs.getDouble("BUCH.PREIS"));
            StudiengangItem sg = new StudiengangItem(rs.getInt("STUDIENGANG.SG_ID"),rs.getString("STUDIENGANG.NAME"));
            HochschulItem hi = new HochschulItem(rs.getInt("HOCHSCHULE.HS_ID"), rs.getString("HOCHSCHULE.NAME"), rs.getString("HOCHSCHULE.ABTEILUNG"), rs.getString("HOCHSCHULE.ORT"),sg);
            PlattformItem i = new PlattformItem(rs.getInt("PLATTFORMITEM.ITEM_ID"), bn, bi, hi, rs.getInt("PLATTFORMITEM.VERKAUFT"));

            list.add(i);
        }
        return list;
    }

    /**
     * Helper um Hochschul-Identity zurückzusetzen
     * @author SmartBooks
     **/
    private void resetStudiengangTabelle() {
        log.trace("reset StudiengangTabelle");

        PreparedStatement stmtRst;

        ResultSet checkForItems;
        ResultSet rs;
        ResultSet rs2;

        try (Connection conn = getConnection())
        {
            //Testen, ob Datenbank leer
            stmtRst = conn.prepareStatement("SELECT COUNT(*) AS ROWCOUNT FROM HOCHSCHULE");
            checkForItems = stmtRst.executeQuery();
            checkForItems.next();
            int testForItems = checkForItems.getInt("ROWCOUNT");

            if(testForItems > 0) {
                //Zwischenspeichern der Studiengangsdaten
                stmtRst = conn.prepareStatement("CREATE TABLE IF NOT EXISTS TEMP_TABLE(SG_ID INT, NAME VARCHAR(40))");
                stmtRst.executeUpdate();
                stmtRst = conn.prepareStatement("INSERT INTO TEMP_TABLE(SG_ID, NAME) SELECT SG_ID, NAME FROM STUDIENGANG");
                stmtRst.executeUpdate();

                //Constraint entfernen
                stmtRst = conn.prepareStatement("ALTER TABLE HOCHSCHULE DROP CONSTRAINT FK_STUDIENGANG");
                stmtRst.executeUpdate();

                //Reset der Studiengangstabelle
                stmtRst = conn.prepareStatement("DELETE FROM STUDIENGANG");
                stmtRst.executeUpdate();
                stmtRst = conn.prepareStatement("ALTER TABLE STUDIENGANG ALTER COLUMN SG_ID RESTART WITH 1");
                stmtRst.executeUpdate();
                stmtRst = conn.prepareStatement("INSERT INTO STUDIENGANG(NAME) SELECT NAME FROM TEMP_TABLE");
                stmtRst.executeUpdate();

                //Neu verlinken der Studiengangsdaten
                stmtRst = conn.prepareStatement("SELECT HS_ID, SG_ID FROM HOCHSCHULE");
                rs = stmtRst.executeQuery();

                while (rs.next()) {
                    int hsID = rs.getInt("HS_ID");
                    int sgID = rs.getInt("SG_ID");

                    stmtRst = conn.prepareStatement("SELECT NAME FROM TEMP_TABLE WHERE SG_ID=?");
                    stmtRst.setInt(1, sgID);
                    rs2 = stmtRst.executeQuery();
                    rs2.next();
                    String sgName = rs2.getString("NAME");

                    stmtRst = conn.prepareStatement("SELECT SG_ID FROM STUDIENGANG WHERE NAME=?");
                    stmtRst.setString(1, sgName);
                    rs2 = stmtRst.executeQuery();
                    rs2.next();
                    sgID = rs2.getInt("SG_ID");

                    stmtRst = conn.prepareStatement("UPDATE HOCHSCHULE SET SG_ID=? WHERE HS_ID=?");
                    stmtRst.setInt(1, sgID);
                    stmtRst.setInt(2, hsID);
                    stmtRst.executeUpdate();
                }

                //Constraints wieder einfügen
                stmtRst = conn.prepareStatement("ALTER TABLE HOCHSCHULE ADD CONSTRAINT FK_STUDIENGANG FOREIGN KEY (SG_ID) REFERENCES STUDIENGANG(SG_ID)");
                stmtRst.executeUpdate();

                //Löschen der Zwischentabelle
                stmtRst = conn.prepareStatement("DROP TABLE TEMP_TABLE");
                stmtRst.executeUpdate();
            } else {
                stmtRst = conn.prepareStatement("ALTER TABLE STUDIENGANG ALTER COLUMN SG_ID RESTART WITH 1");
                stmtRst.executeUpdate();
            }


        } catch (SQLException e) {
            String msg = "SQL error while resetting StudiengangTabelle";
            log.error(msg , e);
            throw new RepositoryException(msg);
        }

    }

    /**
     * Helper um Hochschul-Identity zurückzusetzen
     * @author SmartBooks
     **/
    private void resetHochschulTabelle() {
        log.trace("reset HochschulTabelle");

        PreparedStatement stmtRst;

        ResultSet checkForItems;
        ResultSet rs;
        ResultSet rs2;

        try (Connection conn = getConnection())
        {
            //Testen, ob Datenbank leer
            stmtRst = conn.prepareStatement("SELECT COUNT(*) AS ROWCOUNT FROM HOCHSCHULE");
            checkForItems = stmtRst.executeQuery();
            checkForItems.next();
            int testForItems = checkForItems.getInt("ROWCOUNT");

            if(testForItems > 0) {
                //Zwischenspeichern der Hochschuldaten
                stmtRst = conn.prepareStatement("CREATE TABLE IF NOT EXISTS TEMP_TABLE(HS_ID INT, NAME VARCHAR(40), ABTEILUNG VARCHAR(40), ORT VARCHAR(40), SG_ID INT)");
                stmtRst.executeUpdate();
                stmtRst = conn.prepareStatement("INSERT INTO TEMP_TABLE(HS_ID, NAME, ABTEILUNG, ORT, SG_ID) SELECT HS_ID, NAME, ABTEILUNG, ORT, SG_ID FROM HOCHSCHULE");
                stmtRst.executeUpdate();

                //Constraint entfernen
                stmtRst = conn.prepareStatement("ALTER TABLE PLATTFORMITEM DROP CONSTRAINT FK_HOCHSCHULE");
                stmtRst.executeUpdate();

                //Reset der Hochschultabelle
                stmtRst = conn.prepareStatement("DELETE FROM HOCHSCHULE");
                stmtRst.executeUpdate();
                stmtRst = conn.prepareStatement("ALTER TABLE HOCHSCHULE ALTER COLUMN HS_ID RESTART WITH 1");
                stmtRst.executeUpdate();
                stmtRst = conn.prepareStatement("INSERT INTO HOCHSCHULE(NAME, ABTEILUNG, ORT, SG_ID) SELECT NAME, ABTEILUNG, ORT, SG_ID FROM TEMP_TABLE");
                stmtRst.executeUpdate();

                //Neu verlinken der Hochschuldaten
                stmtRst = conn.prepareStatement("SELECT ITEM_ID, HS_ID FROM PLATTFORMITEM");
                rs = stmtRst.executeQuery();

                while (rs.next()) {
                    int itemID = rs.getInt("ITEM_ID");
                    int hsID = rs.getInt("HS_ID");

                    stmtRst = conn.prepareStatement("SELECT NAME, ABTEILUNG, ORT, SG_ID FROM TEMP_TABLE WHERE HS_ID=?");
                    stmtRst.setInt(1, hsID);
                    rs2 = stmtRst.executeQuery();
                    rs2.next();
                    String hsName = rs2.getString("NAME");
                    String hsAbteilung = rs2.getString("ABTEILUNG");
                    String hsOrt = rs2.getString("ORT");
                    int sgID = rs2.getInt("SG_ID");

                    stmtRst = conn.prepareStatement("SELECT HS_ID FROM HOCHSCHULE WHERE NAME=? AND ABTEILUNG=? AND ORT=? AND SG_ID=?");
                    stmtRst.setString(1, hsName);
                    stmtRst.setString(2, hsAbteilung);
                    stmtRst.setString(3, hsOrt);
                    stmtRst.setInt(4, sgID);
                    rs2 = stmtRst.executeQuery();
                    rs2.next();
                    hsID = rs2.getInt("HS_ID");

                    stmtRst = conn.prepareStatement("UPDATE PLATTFORMITEM SET HS_ID=? WHERE ITEM_ID=?");
                    stmtRst.setInt(1, hsID);
                    stmtRst.setInt(2, itemID);
                    stmtRst.executeUpdate();
                }

                //Constraints wieder einfügen
                stmtRst = conn.prepareStatement("ALTER TABLE PLATTFORMITEM ADD CONSTRAINT FK_HOCHSCHULE FOREIGN KEY (HS_ID) REFERENCES HOCHSCHULE(HS_ID)");
                stmtRst.executeUpdate();

                //Löschen der Zwischentabelle
                stmtRst = conn.prepareStatement("DROP TABLE TEMP_TABLE");
                stmtRst.executeUpdate();
            } else {
                stmtRst = conn.prepareStatement("ALTER TABLE HOCHSCHULE ALTER COLUMN HS_ID RESTART WITH 1");
                stmtRst.executeUpdate();
            }


        } catch (SQLException e) {
            String msg = "SQL error while resetting HochschulTabelle";
            log.error(msg , e);
            throw new RepositoryException(msg);
        }

    }

    /**
     * Helper um Buch-Identity zurückzusetzen
     * @author SmartBooks
     **/
    private void resetBuchTabelle() {
        log.trace("reset BuchTabelle");

        PreparedStatement stmtRst;

        ResultSet checkForItems;
        ResultSet rs;
        ResultSet rs2;

        try (Connection conn = getConnection())
        {
            //Testen, ob Datenbank leer
            stmtRst = conn.prepareStatement("SELECT COUNT(*) AS ROWCOUNT FROM BUCH");
            checkForItems = stmtRst.executeQuery();
            checkForItems.next();
            int testForItems = checkForItems.getInt("ROWCOUNT");

            if(testForItems > 0) {
                //Zwischenspeichern der Buchdaten
                stmtRst = conn.prepareStatement("CREATE TABLE IF NOT EXISTS TEMP_TABLE(B_ID INT, TITEL VARCHAR(50), ISBN VARCHAR(20), AUTHOR VARCHAR(30), VEROEFF_DAT DATE, VERSION DOUBLE, PREIS DOUBLE)");
                stmtRst.executeUpdate();
                stmtRst = conn.prepareStatement("INSERT INTO TEMP_TABLE(B_ID, TITEL, ISBN, AUTHOR, VEROEFF_DAT, VERSION, PREIS) SELECT B_ID, TITEL, ISBN, AUTHOR, VEROEFF_DAT, VERSION, PREIS FROM BUCH");
                stmtRst.executeUpdate();

                //Constraint entfernen
                stmtRst = conn.prepareStatement("ALTER TABLE PLATTFORMITEM DROP CONSTRAINT FK_BUCH");
                stmtRst.executeUpdate();

                //Reset der Buchtabelle
                stmtRst = conn.prepareStatement("DELETE FROM BUCH");
                stmtRst.executeUpdate();
                stmtRst = conn.prepareStatement("ALTER TABLE BUCH ALTER COLUMN B_ID RESTART WITH 1");
                stmtRst.executeUpdate();
                stmtRst = conn.prepareStatement("INSERT INTO BUCH(TITEL, ISBN, AUTHOR, VEROEFF_DAT, VERSION, PREIS) SELECT TITEL, ISBN, AUTHOR, VEROEFF_DAT, VERSION, PREIS FROM TEMP_TABLE");
                stmtRst.executeUpdate();

                //Neu verlinken der Buchdaten
                stmtRst = conn.prepareStatement("SELECT ITEM_ID, B_ID FROM PLATTFORMITEM");
                rs = stmtRst.executeQuery();

                while (rs.next()) {
                    int itemID = rs.getInt("ITEM_ID");
                    int bID = rs.getInt("B_ID");

                    stmtRst = conn.prepareStatement("SELECT TITEL, ISBN, AUTHOR, VEROEFF_DAT, VERSION, PREIS FROM TEMP_TABLE WHERE B_ID=?");
                    stmtRst.setInt(1, bID);
                    rs2 = stmtRst.executeQuery();
                    rs2.next();
                    String bTitel = rs2.getString("TITEL");
                    String bIsbn = rs2.getString("ISBN");
                    String bAuthor = rs2.getString("AUTHOR");
                    LocalDate bVeroeff = rs2.getDate("VEROEFF_DAT").toLocalDate();
                    double bVersion = rs2.getDouble("VERSION");
                    double bPreis = rs2.getDouble("PREIS");

                    stmtRst = conn.prepareStatement("SELECT B_ID FROM BUCH WHERE TITEL=? AND ISBN=? AND AUTHOR=? AND VEROEFF_DAT=? AND VERSION=? AND PREIS=?");
                    stmtRst.setString(1, bTitel);
                    stmtRst.setString(2, bIsbn);
                    stmtRst.setString(3, bAuthor);
                    stmtRst.setDate(4, java.sql.Date.valueOf(bVeroeff));
                    stmtRst.setDouble(5, bVersion);
                    stmtRst.setDouble(6, bPreis);
                    rs2 = stmtRst.executeQuery();
                    rs2.next();
                    bID = rs2.getInt("B_ID");

                    stmtRst = conn.prepareStatement("UPDATE PLATTFORMITEM SET B_ID=? WHERE ITEM_ID=?");
                    stmtRst.setInt(1, bID);
                    stmtRst.setInt(2, itemID);
                    stmtRst.executeUpdate();
                }

                //Constraints wieder einfügen
                stmtRst = conn.prepareStatement("ALTER TABLE PLATTFORMITEM ADD CONSTRAINT FK_BUCH FOREIGN KEY (B_ID) REFERENCES BUCH(B_ID)");
                stmtRst.executeUpdate();

                //Löschen der Zwischentabelle
                stmtRst = conn.prepareStatement("DROP TABLE TEMP_TABLE");
                stmtRst.executeUpdate();
            } else {
                stmtRst = conn.prepareStatement("ALTER TABLE BUCH ALTER COLUMN B_ID RESTART WITH 1");
                stmtRst.executeUpdate();
            }


        } catch (SQLException e) {
            String msg = "SQL error while resetting BuchTabelle";
            log.error(msg , e);
            throw new RepositoryException(msg);
        }

    }

    /**
     * Helper um Benutzer-Identity zurückzusetzen
     * @author SmartBooks
     **/
    private void resetBenutzerTabelle() {
        log.trace("reset BenutzerTabelle");

        PreparedStatement stmtRst;

        ResultSet checkForItems;
        ResultSet rs;
        ResultSet rs2;

        try (Connection conn = getConnection())
        {
            //Testen, ob Datenbank leer
            stmtRst = conn.prepareStatement("SELECT COUNT(*) AS ROWCOUNT FROM BENUTZER");
            checkForItems = stmtRst.executeQuery();
            checkForItems.next();
            int testForItems = checkForItems.getInt("ROWCOUNT");

            if(testForItems > 0) {
                //Zwischenspeichern der Benutzerdaten
                stmtRst = conn.prepareStatement("CREATE TABLE IF NOT EXISTS TEMP_TABLE(BEN_ID INT, NAME VARCHAR(40), EMAIL VARCHAR(256))");
                stmtRst.executeUpdate();
                stmtRst = conn.prepareStatement("INSERT INTO TEMP_TABLE(BEN_ID, NAME, EMAIL) SELECT BEN_ID, NAME, EMAIL FROM BENUTZER");
                stmtRst.executeUpdate();

                //Constraint entfernen
                stmtRst = conn.prepareStatement("ALTER TABLE PLATTFORMITEM DROP CONSTRAINT FK_BENUTZER");
                stmtRst.executeUpdate();

                //Reset der Benutzertabelle
                stmtRst = conn.prepareStatement("DELETE FROM BENUTZER");
                stmtRst.executeUpdate();
                stmtRst = conn.prepareStatement("ALTER TABLE BENUTZER ALTER COLUMN BEN_ID RESTART WITH 1");
                stmtRst.executeUpdate();
                stmtRst = conn.prepareStatement("INSERT INTO BENUTZER(NAME, EMAIL) SELECT NAME, EMAIL FROM TEMP_TABLE");
                stmtRst.executeUpdate();

                //Neu verlinken der Benutzerdaten
                stmtRst = conn.prepareStatement("SELECT ITEM_ID, BEN_ID FROM PLATTFORMITEM");
                rs = stmtRst.executeQuery();

                while (rs.next()) {
                    int itemID = rs.getInt("ITEM_ID");
                    int benID = rs.getInt("BEN_ID");

                    stmtRst = conn.prepareStatement("SELECT NAME, EMAIL FROM TEMP_TABLE WHERE BEN_ID=?");
                    stmtRst.setInt(1, benID);
                    rs2 = stmtRst.executeQuery();
                    rs2.next();
                    String benName = rs2.getString("NAME");
                    String benEmail = rs2.getString("EMAIL");

                    stmtRst = conn.prepareStatement("SELECT BEN_ID FROM BENUTZER WHERE NAME=? AND EMAIL=?");
                    stmtRst.setString(1, benName);
                    stmtRst.setString(2, benEmail);
                    rs2 = stmtRst.executeQuery();
                    rs2.next();
                    benID = rs2.getInt("BEN_ID");

                    stmtRst = conn.prepareStatement("UPDATE PLATTFORMITEM SET BEN_ID=? WHERE ITEM_ID=?");
                    stmtRst.setInt(1, benID);
                    stmtRst.setInt(2, itemID);
                    stmtRst.executeUpdate();
                }

                //Constraints wieder einfügen
                stmtRst = conn.prepareStatement("ALTER TABLE PLATTFORMITEM ADD CONSTRAINT FK_BENUTZER FOREIGN KEY (BEN_ID) REFERENCES BENUTZER(BEN_ID)");
                stmtRst.executeUpdate();

                //Löschen der Zwischentabelle
                stmtRst = conn.prepareStatement("DROP TABLE TEMP_TABLE");
                stmtRst.executeUpdate();
            } else {
                stmtRst = conn.prepareStatement("ALTER TABLE BENUTZER ALTER COLUMN BEN_ID RESTART WITH 1");
                stmtRst.executeUpdate();
            }


        } catch (SQLException e) {
            String msg = "SQL error while resetting BenutzerTabelle";
            log.error(msg , e);
            throw new RepositoryException(msg);
        }

    }

    /**
     * Helper um PlattformItem-Identity zurückzusetzen
     * @author SmartBooks
     **/
    private void resetPlattformItemTabelle() {
        log.trace("reset PlattformItemTabelle");

        PreparedStatement stmtRst;

        ResultSet checkForItems;
        ResultSet rs;
        ResultSet rs2;

        try (Connection conn = getConnection()) {

            //Testen, ob Datenbank leer
            stmtRst = conn.prepareStatement("SELECT COUNT(*) AS ROWCOUNT FROM PLATTFORMITEM");
            checkForItems = stmtRst.executeQuery();
            checkForItems.next();
            int testForItems = checkForItems.getInt("ROWCOUNT");

            if(testForItems > 0) {
                //Zwischenspeichern der PlattformItem-Daten
                stmtRst = conn.prepareStatement("CREATE TABLE IF NOT EXISTS TEMP_TABLE(ITEM_ID INT, BEN_ID INT, B_ID INT, HS_ID INT, VERKAUFT BIT)");
                stmtRst.executeUpdate();
                stmtRst = conn.prepareStatement("INSERT INTO TEMP_TABLE(ITEM_ID, BEN_ID, B_ID, HS_ID, VERKAUFT) SELECT ITEM_ID, BEN_ID, B_ID, HS_ID, VERKAUFT FROM PLATTFORMITEM");
                stmtRst.executeUpdate();

                //Reset der PlattformItem-Tabelle
                stmtRst = conn.prepareStatement("DELETE FROM PLATTFORMITEM");
                stmtRst.executeUpdate();
                stmtRst = conn.prepareStatement("ALTER TABLE PLATTFORMITEM ALTER COLUMN ITEM_ID RESTART WITH 1");
                stmtRst.executeUpdate();
                stmtRst = conn.prepareStatement("INSERT INTO PLATTFORMITEM(BEN_ID, B_ID, HS_ID, VERKAUFT) SELECT BEN_ID, B_ID, HS_ID, VERKAUFT FROM TEMP_TABLE");
                stmtRst.executeUpdate();

                //Löschen der Zwischentabelle
                stmtRst = conn.prepareStatement("DROP TABLE TEMP_TABLE");
                stmtRst.executeUpdate();
            } else {
                stmtRst = conn.prepareStatement("ALTER TABLE PLATTFORMITEM ALTER COLUMN ITEM_ID RESTART WITH 1");
                stmtRst.executeUpdate();
            }


        } catch (SQLException e) {
            String msg = "SQL error while resetting PlattformItemTabelle";
            log.error(msg, e);
            throw new RepositoryException(msg);
        }
    }
}
