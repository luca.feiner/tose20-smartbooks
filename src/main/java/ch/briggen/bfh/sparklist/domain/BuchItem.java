package ch.briggen.bfh.sparklist.domain;


import java.time.LocalDate;

/**
 * @author SmartBooks
 */
public class BuchItem {

    private int buchID;
    private String titel;
    private String isbn;
    private String author;
    private LocalDate veroeff_dat;
    private double version;
    private double preis;

    /**
     * Defaultkonstruktor für die Verwendung in einem Controller
     */
    public BuchItem()
    {

    }

    /**
     * Konstruktor
     * @param buchID Eindeutige Id
     */
    public BuchItem(int buchID, String titel, String isbn, String author, LocalDate veroeff_dat, double version, double preis)
    {
        this.buchID = buchID;
        this.titel = titel;
        this.isbn = isbn;
        this.author = author;
        this.veroeff_dat = veroeff_dat;
        this.version = version;
        this.preis = preis;
    }

    public int getBuchId() {
        return buchID;
    }

    public void setBuchId(int buchID) {
        this.buchID = buchID;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getISBN() {
        return isbn;
    }

    public void setISBN(String isbn) {
        this.isbn = isbn;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public LocalDate getVeroeff_dat() {
        return veroeff_dat;
    }

    public void setVeroeff_dat(LocalDate veroeff_dat) {
        this.veroeff_dat = veroeff_dat;
    }

    public double getVersion(){
        return version;
    }

    public void setVersion(double version) {
        this.version = version;
    }

    public double getPreis() {
        return preis;
    }

    public void setPreis(double preis) {
        this.preis = preis;
    }

    @Override
    public String toString() {
        return String.format("BuchItem:{buchID: %d; titel: %s; isbn: %s; author: %s; veroeff_dat: %s; version: %3.1f; preis: %4.1f}", buchID, titel, isbn, author, veroeff_dat, version, preis);
    }

    public boolean isEqual(BuchItem bi) {
        if(this.titel.equals(bi.titel)) {
            if(this.isbn.equals(bi.isbn)) {
                if(this.author.equals(bi.author)) {
                    if(this.veroeff_dat.equals(bi.veroeff_dat)) {
                        if(this.version == bi.version) {
                            if(this.preis == bi.preis) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

}
