package ch.briggen.bfh.sparklist.domain;

/**
 * @author SmartBooks
 */

public class BenutzerItem {
    private int benutzerId;
    private String email;
    private String name;

    /**
     * Defaultkonstruktor für die Verwendung in einem Controller
     */
    public BenutzerItem()
    {

    }

    /**
     * Konstruktor für BuchItem
     */
    public BenutzerItem(int benutzerId, String email, String name)
    {
        this.benutzerId = benutzerId;
        this.email = email;
        this.name = name;
    }

    public int getBenutzerId() {
        return benutzerId;
    }

    public void setBenutzerId(int benutzerId) {
        this.benutzerId = benutzerId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBenutzerName() {
        return name;
    }

    public void setBenutzerName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("Benutzer:{id: %d; name: %s; email: %s;}", benutzerId, name, email);
    }

    /**
     * Überprüfung ob ein Benutzername mit einem anderen identisch ist.
     * @param bi
     * @return
     */
    public boolean isEqual(BenutzerItem bi) {
        if(this.name.equals(bi.name)) {
            if(this.email.equals(bi.email)) {
                return true;
            }
        }
        return false;
    }

}
