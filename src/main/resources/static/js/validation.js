function validateForm(event){
    var form = document.forms["form"];
    var fields = [['titel', 'TITEL'], ['isbn', 'ISBN'], ['author', 'AUTHOR'], ['veroeff_dat', 'VERÖFFENTLICHKEITS DATUM'], ['version', 'VERSION'], ['preis', 'PREIS'], ['hochschule', 'HOCHSCHULE'], ['hs_abteilung', 'HOCHSCHULABTEILUNG'], ['hs_ort', 'HOCHSCHULORT'], ['studiengang', 'STUDIENGANG'], ['benutzername', 'BENUTZERNAME'], ['email', 'EMAIL']];
    var stringFields = [['author', 'AUTHOR'], ['hochschule', 'HOCHSCHULE'], ['hs_abteilung', 'HOCHSCHULABTEILUNG'], ['hs_ort', 'HOCHSCHULORT'], ['studiengang', 'STUDIENGANG']];
    var integerFields = [['isbn', 'ISBN'], ['version', 'VERSION'], ['preis', 'PREIS']];
    var content = document.getElementById("message");
    var errorsEmpty = [];
    var errorsNumber = [];
    var errorsString = [];
    console.log(fields);

    errorsEmpty = checkIfNotEmpty(fields);
    errorsNumber = checkIfString(stringFields);
    errorsString = checkIfNumber(integerFields);



    if(errorsEmpty.length > 0 || errorsNumber.length > 0 || errorsString.length > 0){
    var msg = "<div>";
        event.preventDefault();
        event.stopPropagation();

        if(errorsEmpty.length > 0) {
            msg += "<p> Diese Felder müssen ausgefüllt sein: <ul> \n";
            errorsEmpty.forEach(function(item, index,array){
                msg += "<li>" + item.toUpperCase() + "</li>";
            });
            msg += "</ul></p>";
        }

        if(errorsNumber.length > 0) {
        msg += "<p> Diese Felder dürfen keine Zahlen und Sonderzeichen enthalten (Umlaute müssen ausgeschrieben werden. Bsp ä = ae): <ul> \n";
            errorsNumber.forEach(function(item, index,array){
                msg += "<li>" + item.toUpperCase() + "</li>";
            });
            msg += "</ul></p>";
        }
        if(errorsString.length > 0) {
            msg += "<p> Diese Felder dürfen keine Buchstaben enthalten: <ul> \n";
            errorsString.forEach(function(item, index,array){
                msg += "<li>" + item.toUpperCase() + "</li>";
            });
            msg += "</ul></p></div>";
        }


        content.innerHTML = msg;
    }

    if(ValidateEmail(form["PlattformItem.email"].value) == false){
        event.preventDefault();
        event.stopPropagation();
    }




}
function ValidateEmail(mail)
    {
     if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
      {
        return (true)
      }
        alert("You have entered an invalid email address!")
        return (false);
    }

function checkIfNotEmpty(fields){
    var e = []
    fields.forEach(function(item, index, array) {
      if(form["PlattformItem." + item[0]].value == ""){
        e[index] = item[1];
      }
    });
return e;
}

function checkIfString(fields){
    var e = [];
    var x = 0;
        fields.forEach(function(item, index, array) {
        if(form["PlattformItem." + item[0]].value){
          if(/^([a-zA-Z\s])+$/g.test(form["PlattformItem." + item[0]].value) == false){
            e[x++] = item[1];
          }
        }
        });
    return e;
}

function checkIfNumber(fields){
    var e = [];
    var x = 0;
        fields.forEach(function(item, index, array) {
        if(form["PlattformItem." + item[0]].value != ""){
          if(/^[[0-9\.]+$/g.test(form["PlattformItem." + item[0]].value) == false){
            e[x++] = item[1];
          }

        }
        });
    return e;
}